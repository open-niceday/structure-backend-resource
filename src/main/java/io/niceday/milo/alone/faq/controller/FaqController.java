package io.niceday.milo.alone.faq.controller;

import io.niceday.milo.alone.faq.form.FaqForm;
import io.niceday.milo.alone.faq.predicate.FaqPredicate;
import io.niceday.milo.alone.faq.service.FaqService;
import io.niceday.milo.alone.faq.form.FaqForm.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static io.niceday.milo.alone.faq.mapper.FaqMapper.mapper;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description faq controller
 **********************************************************************************************************************/
@Api(description="자주묻는질문")
@RestController
@RequestMapping("${property.api.end-point}")
@RequiredArgsConstructor
public class FaqController {
    
    private final FaqService faqService;

    @ApiOperation("목록")
    @GetMapping("/faqs/pages")
    public Page<Response.FindAll> getPage(@Valid FaqForm.Request.Find find, @PageableDefault Pageable pageable) {
        return faqService.getPage(FaqPredicate.search(find), pageable).map(mapper::toFindAll);
    }
    
    @ApiOperation("조회")
    @GetMapping("/faqs/{faqId}")
    public Response.FindOne get(@PathVariable Long faqId) {
        return mapper.toFindOne(faqService.get(faqId));
    }
    
    @ApiOperation("등록")
    @PostMapping("/faqs")
    public Response.FindOne add(@Valid @RequestBody FaqForm.Request.Add add){
        return mapper.toFindOne(faqService.add(mapper.toEntity(add)));
    }
    
    @ApiOperation("수정")
    @PutMapping("/faqs/{faqId}")
    public Response.FindOne modify(@PathVariable Long faqId, @Valid @RequestBody FaqForm.Request.Modify modify){
        return mapper.toFindOne(faqService.modify(mapper.toEntity(modify, faqId), faqId));
    }
    
    @ApiOperation("삭제")
    @DeleteMapping("/faqs/{faqId}")
    public void remove(@PathVariable Long faqId){
        faqService.remove(faqId);
    }

    @ApiOperation("변경-순서")
    @PatchMapping("/faqs/change-sort")
    public Response.FindOne changeSort(@Valid @RequestBody FaqForm.Request.ChangeSort sort){
        return mapper.toFindOne(faqService.changeSort(sort.getSourceId(), sort.getTargetId()));
    }
}
