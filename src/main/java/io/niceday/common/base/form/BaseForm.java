package io.niceday.common.base.form;

import io.niceday.milo.alone.attach.validator.AttachValidator;
import io.niceday.milo.alone.image.validator.ImageValidator;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description base form
 **********************************************************************************************************************/
public class BaseForm {

	public static class Request {

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Attach {

			@ApiModelProperty(value="첨부일련번호", required=true)
			@NotBlank
			private String id;

			@ApiModelProperty(value="첨부검증", hidden=true)
			@AssertTrue
			public boolean isValidAttach() {
				return AttachValidator.isValid(id);
			}
		}

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Image {

			@ApiModelProperty(value="이미지일련번호", required=true)
			@NotBlank
			private String id;

			@ApiModelProperty(value="이미지검증", hidden=true)
			@AssertTrue
			public boolean isValidImage() {
				return ImageValidator.isValid(id);
			}
		}
	}

	public static class Response {

		@Data
		public static class Account {

			@ApiModelProperty(value="사용자일련번호")
			private Long id;

			@ApiModelProperty(value="이름")
			private String name;
		}

		@Data
		public static class Attach {

			@ApiModelProperty(value="첨부일련번호")
			private String id;

			@ApiModelProperty(value="이름")
			private String name;

			@ApiModelProperty(value="경로")
			private String path;

			@ApiModelProperty(value="크기")
			private Long size;
		}

		@Data
		public static class Image {

			@ApiModelProperty(value="이미지일련번호")
			private String id;

			@ApiModelProperty(value="이름")
			private String name;

			@ApiModelProperty(value="경로")
			private String path;

			@ApiModelProperty(value="크기")
			private Long size;
		}
	}
}