package io.niceday.milo.alone.attach.mapper;

import io.niceday.milo.alone.attach.entity.Attach;
import io.niceday.milo.alone.attach.form.AttachForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description attach mapper
 **********************************************************************************************************************/
@Mapper
public interface AttachMapper {

	AttachMapper mapper = Mappers.getMapper(AttachMapper.class);

	Response.FindOne toFindOne(Attach entity);
	Response.FindAll toFindAll(Attach entity);
}
