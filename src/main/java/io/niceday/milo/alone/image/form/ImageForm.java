package io.niceday.milo.alone.image.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description image form
 **********************************************************************************************************************/
public class ImageForm {

	public static class Response {

		@Data
		public static class FindAll {

			@ApiModelProperty(value="이미지일련번호")
			private String id;

			@ApiModelProperty(value="원본이름")
			private String name;

			@ApiModelProperty(value="경로")
			private String path;

			@ApiModelProperty(value="크기")
			private Long size;
		}

		@Data
		public static class FindOne {

			@ApiModelProperty(value="이미지일련번호")
			private String id;

			@ApiModelProperty(value="원본이름")
			private String name;

			@ApiModelProperty(value="경로")
			private String path;

			@ApiModelProperty(value="크기")
			private Long size;
		}
	}
}
