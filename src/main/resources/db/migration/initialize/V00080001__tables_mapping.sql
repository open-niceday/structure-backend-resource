create table faq_attach (
     faq_id    bigint      not null
    ,attach_id varchar(50) not null
    ,primary key(faq_id, attach_id)
    ,foreign key(faq_id)    references faq(id)
    ,foreign key(attach_id) references attach(id)
);

create table notice_attach (
     notice_id bigint      not null
    ,attach_id varchar(50) not null
    ,primary key(notice_id, attach_id)
    ,foreign key(notice_id) references notice(id)
    ,foreign key(attach_id) references attach(id)
);

create table question_attach (
     question_id bigint      not null
    ,attach_id   varchar(50) not null
    ,primary key(question_id, attach_id)
    ,foreign key(question_id) references question(id)
    ,foreign key(attach_id)   references attach  (id)
);

create table comment_question (
     comment_id  bigint not null
    ,question_id bigint not null
    ,primary key(comment_id, question_id)
    ,foreign key(comment_id)  references comment (id)
    ,foreign key(question_id) references question(id)
);