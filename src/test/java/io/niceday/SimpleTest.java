package io.niceday;

import java.util.regex.Pattern;

public class SimpleTest {

    public static void main(String[] args) {

        System.out.println("---------------------- 개인정보 - 주민등록번호 --------------------");
        Pattern pattern = Pattern.compile("(\\d{6}\\D?\\d)(\\d{6})");
        System.out.println(pattern.matcher("820121-1666310").find());
        System.out.println(pattern.matcher("8201211666310").find());
        System.out.println(pattern.matcher("한8201211666310").find());
        System.out.println(pattern.matcher("8201211666310한").find());
        System.out.println(pattern.matcher("한8201211666310힌").find());
        System.out.println(pattern.matcher("한820121-1666310힌").find());
        System.out.println(pattern.matcher("한8201211-666310힌").find());
    }
}
