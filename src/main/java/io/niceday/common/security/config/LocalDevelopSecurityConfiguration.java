package io.niceday.common.security.config;//package io.niceday.common.security.config;
//
//import com.google.common.collect.ImmutableList;
//import io.niceday.common.engine.constant.Constant;
//import io.niceday.common.engine.helper.model.ObjectHelper;
//import io.niceday.milo.access.account.form.AccountForm;
//import io.niceday.milo.access.role.enumerate.RoleType;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.stereotype.Component;
//
///**
// * @since       2021.04.16
// * @author      lucas
// * @description local develop security configuration
// **********************************************************************************************************************/
//@Configuration
//@EnableWebSecurity
//@Profile({"default", "test"})
//public class LocalDevelopSecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/**");
//    }
//}
//
//@Aspect
//@Component
//@Profile({"default", "test"})
//class LocalDevelopSecurityContext {
//
//    @Before("execution(* kr.co.genie..controller..*(..))")
//    public void before(JoinPoint p) throws Throwable {
//
//        AccountForm.Response.Principal principal =
//                AccountForm.Response.Principal.builder()
//                        .id    (1L)
//                        .name  ("임현우")
//                        .email ("lucas@niceday.io")
//                        .build();
//
//        SecurityContextHolder.getContext().setAuthentication(
//                new UsernamePasswordAuthenticationToken(ObjectHelper.toJson(principal), Constant.String.EMPTY, ImmutableList.of(getGrantedAuthority(RoleType.ROLE_ADMIN), getGrantedAuthority(RoleType.ROLE_USER), getGrantedAuthority(RoleType.ROLE_ANONYMOUS)))
//        );
//    }
//
//    private GrantedAuthority getGrantedAuthority(RoleType roleType){
//        return new SimpleGrantedAuthority(roleType.name());
//    }
//}
