package io.niceday.milo.alone.comment.form;


import io.niceday.common.base.form.BaseForm;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @since       2021.11.30
 * @author      lucas
 * @description comment form
 **********************************************************************************************************************/
public class CommentForm {

    public static class Request {

        @Setter
        @Getter
        @ToString
        @Builder(toBuilder=true)
        @AllArgsConstructor
        @NoArgsConstructor
        public static class Add {

            @ApiModelProperty(value="내용", required=true)
            @NotBlank
            private String content;
        }
    }

    public static class Response {

        @Data
        public static class FindAll {

            @ApiModelProperty(value="댓글일련번호")
            private Long id;

            @ApiModelProperty(value="내용")
            private String content;

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }

        @Data
        public static class FindOne {

            @ApiModelProperty(value="댓글일련번호")
            private Long id;

            @ApiModelProperty(value="내용")
            private String content;

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }
    }
}
