package io.niceday.milo.alone.faq.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.milo.alone.attach.entity.Attach;
import io.niceday.milo.alone.faq.enumerate.FaqType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description faq
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("자주묻는질문")
public class Faq extends Base {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Description("자주묻는질문일련번호")
	@Column(nullable=false)
	private Long id;

	@Description("자주묻는질문타입")
	@Enumerated(EnumType.STRING)
	@Column(nullable=false, length=30)
	private FaqType faqType;

	@Description("제목")
	@Column(nullable=false, length=1000)
	private String title;

	@Description("내용")
	@Column(nullable=false, length=60000)
	private String content;

	@Description("순서")
	@Column(nullable=false)
	private Long sort;


	@Description("첨부")
	@OneToOne(fetch=FetchType.LAZY)
	@JoinTable(name="faq_attach", joinColumns=@JoinColumn(name="faq_id"), inverseJoinColumns=@JoinColumn(name="attach_id"))
	private Attach attach;
}