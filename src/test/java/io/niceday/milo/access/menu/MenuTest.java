package io.niceday.milo.access.menu;

import io.niceday.common.engine.test.SuperTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import static io.niceday.milo.access.menu.MenuHelper.getMenus;

/**
 * @since       2022.11.29
 * @author      martin
 * @description menu test
 **********************************************************************************************************************/
@Transactional
@DisplayName("메뉴")
public class MenuTest extends SuperTest {

	@Test
	@DisplayName("01_목록")
	public void t01_getMenus() {
		getMenus();
	}
}
