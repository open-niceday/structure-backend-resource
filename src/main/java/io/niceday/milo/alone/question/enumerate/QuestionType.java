package io.niceday.milo.alone.question.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description question type
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum QuestionType {

     ALL      ("전체")
    ,CALCULATE("정산")
    ,SELL     ("판매/프로모션")
    ,SERVICE  ("서비스이용");

    private String description;
}
