package io.niceday.milo.alone.notice.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.milo.alone.attach.entity.Attach;
import io.niceday.milo.alone.notice.enumerate.NoticeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description notice
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("공지")
public class Notice extends Base {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Description("공지일련번호")
	@Column(nullable=false)
	private Long id;

	@Description("공지타입")
	@Enumerated(EnumType.STRING)
	@Column(nullable=false, length=30)
	private NoticeType noticeType;

	@Description("제목")
	@Column(nullable=false, length=1000)
	private String title;

	@Description("내용")
	@Column(nullable=false, length=60000)
	private String content;


	@Description("첨부")
	@OneToOne(fetch=FetchType.LAZY)
	@JoinTable(name="notice_attach", joinColumns=@JoinColumn(name="notice_id"), inverseJoinColumns=@JoinColumn(name="attach_id"))
	private Attach attach;
}