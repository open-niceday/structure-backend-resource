package io.niceday.milo.alone.comment.adapter;

import com.querydsl.core.types.Predicate;
import io.niceday.common.engine.exception.common.ExceptionCode;
import io.niceday.common.engine.exception.common.NotAcceptableException;
import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.milo.alone.comment.entity.Comment;
import io.niceday.milo.alone.comment.predicate.CommentPredicate;
import io.niceday.milo.alone.comment.service.CommentService;
import io.niceday.milo.alone.question.entity.Question;
import io.niceday.milo.alone.question.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @since       2022.12.01
 * @author      lucas
 * @description comment adapter
 **********************************************************************************************************************/
@Component
@Transactional
@RequiredArgsConstructor
public class CommentAdapter {

    private final CommentService  commentService;
    private final QuestionService questionService;

    public List<Comment> getAll(Predicate predicate) {
        return commentService.getAll(predicate);
    }

    public Comment add(Long questionId, Comment comment) {

        if(CollectionUtils.isNotEmpty(getAll(CommentPredicate.question(questionId)))){
            throw new NotAcceptableException(ExceptionCode.E00010003);
        }

        Question question = questionService.get(questionId);
        question.setAnswerYn(Boolean.TRUE);

        comment.setQuestion(question);
        return commentService.add(comment);
    }

    public void remove(Long questionId, Long commentId) {

        Question question = questionService.get(questionId);
        question.setAnswerYn(Boolean.FALSE);

        Comment comment = commentService.get(commentId);
        if(ObjectUtils.notEqual(question.getId(), comment.getQuestion().getId())){
            throw new NotFoundException();
        }

        commentService.remove(commentId);
    }
}