package io.niceday.common.base.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2021.03.19
 * @author      lucas
 * @description display status
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum DisplayStatus {

     NOW    ("즉시")
    ,RESERVE("예약")
    ,DISABLE("미노출");

    private String description;
}