package io.niceday.common.security.principal.form;

import io.niceday.milo.access.account.enumerate.AccountStatus;
import io.niceday.milo.access.role.enumerate.RoleType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @since       2022.11.15
 * @author      lucas
 * @description principal form
 **********************************************************************************************************************/
public class PrincipalForm {

	@Data
	public static class Principal {

		@ApiModelProperty(value="사용자일련번호")
		private Long id;

		@ApiModelProperty(value="사용자상태")
		private AccountStatus accountStatus;

		@ApiModelProperty(value="이름")
		private String name;

		@ApiModelProperty(value="이메일")
		private String email;

		@ApiModelProperty(value="권한목록")
		private List<Role> roles;

		@Data
		public static class Role {

			@ApiModelProperty(value="권한일련번호")
			private Long id;

			@ApiModelProperty(value="이름")
			private RoleType roleType;

			@ApiModelProperty(value="설명")
			private String description;
		}
	}
}
