package io.niceday.milo.alone.question.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import io.niceday.milo.alone.question.entity.QQuestion;
import io.niceday.milo.alone.question.form.QuestionForm.Request;

import java.util.Optional;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description question predicate
 **********************************************************************************************************************/
public class QuestionPredicate {

   public static Predicate search(Request.Find find){

       QQuestion      question = QQuestion.question;
       BooleanBuilder builder  = new BooleanBuilder();

       Optional.ofNullable(find.getQuestionType()).ifPresent(p -> builder.and(question.questionType.eq(p)));
       Optional.ofNullable(find.getKeyword     ()).ifPresent(p -> builder.and(question.title.startsWith(p).or(question.creator.name.startsWith(p))));

       return builder;
   }
}
