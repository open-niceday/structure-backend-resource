package io.niceday.common.engine.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;

/**
 * @since       2021.02.01
 * @author      lucas
 * @description super test
 **********************************************************************************************************************/
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
@ExtendWith({SpringExtension.class, RestDocumentationExtension.class})
@Disabled
public class SuperTest {

	           public MockMvc 						 mock;
	           public RestDocumentationResultHandler handler;
	@Autowired public WebApplicationContext          context;

	@BeforeEach
	public void superBefore(RestDocumentationContextProvider restDocumentationContextProvider) {
		handler = MockMvcRestDocumentation.document(
				"{class-name}/{method-name}"
				, preprocessRequest(prettyPrint())
				, preprocessResponse(prettyPrint()));

		mock = MockMvcBuilders.webAppContextSetup(context)
				.apply(documentationConfiguration(restDocumentationContextProvider))
				.alwaysDo(handler)
				.apply(SecurityMockMvcConfigurers.springSecurity())
				.build();

		TestHelper.initialize(mock, handler);
	}
}
