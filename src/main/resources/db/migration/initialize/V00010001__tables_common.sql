create table image (
     id            varchar(50)  not null
    ,name          varchar(500) not null
    ,path          varchar(300) not null
    ,size          bigint       not null
    ,created_by    bigint       not null
    ,created_at    datetime     not null
    ,updated_by    bigint
    ,updated_at    datetime
    ,primary key(id)
) auto_increment = 10000;

create table attach (
     id            varchar(50)  not null
    ,name          varchar(500) not null
    ,path          varchar(300) not null
    ,size          bigint       not null
    ,created_by    bigint       not null
    ,created_at    datetime     not null
    ,updated_by    bigint
    ,updated_at    datetime
    ,primary key(id)
) auto_increment = 10000;