package io.niceday.milo.access.menu.service;

import io.niceday.milo.access.menu.entity.Menu;
import io.niceday.milo.access.menu.repository.MenuRepository;
import io.niceday.milo.access.role.entity.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @since       2022.06.02
 * @author      mushi
 * @description menu service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class MenuService {

    private final MenuRepository menuRepository;

    @Transactional(readOnly=true)
    public List<Menu> getMenus(List<Role> roles) {
        return menuRepository.findDistinctByParentIsNullAndRolesInOrderBySortAsc(roles);
    }
}
