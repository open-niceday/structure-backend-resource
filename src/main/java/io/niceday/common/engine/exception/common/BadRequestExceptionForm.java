package io.niceday.common.engine.exception.common;

import io.swagger.annotations.ApiModelProperty;
import io.niceday.common.engine.helper.message.MessageHelper;
import lombok.*;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @since       2021.01.26
 * @author      preah
 * @description bad request form
 **********************************************************************************************************************/
@Getter
@Setter
@Builder(toBuilder=true)
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BadRequestExceptionForm {

    @ApiModelProperty(required=true, value="응답 코드")
    private ExceptionCode code;

    @ApiModelProperty(required=true, value="메시지 내용")
    private String        message;

    @ApiModelProperty(value="필드별 상세")
	private List<Error>   errors;

	@Getter
	@Setter
	@Builder(toBuilder=true)
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Error {

		private String       field;
		private List<String> messages;
	}

	public static BadRequestExceptionForm create(BindException exception, ExceptionCode code) {
		return toBadRequest(exception.getBindingResult(), code);
	}

	public static BadRequestExceptionForm create(MethodArgumentNotValidException exception, ExceptionCode code) {
		return toBadRequest(exception.getBindingResult(), code);
	}

	private static BadRequestExceptionForm toBadRequest(BindingResult bindingResult, ExceptionCode code) {
		BadRequestExceptionForm form = BadRequestExceptionForm.builder()
				.code(code)
				.message(MessageHelper.getMessage(code))
				.build();

		if(CollectionUtils.isNotEmpty(bindingResult.getFieldErrors())) {

			List<FieldError> errors = bindingResult.getFieldErrors();
			form.setErrors(errors.stream()
					.map     (FieldError::getField)
					.distinct()
					.map     (field -> Error.builder().field(field).build())
					.collect (Collectors.toList()));

			form.getErrors().forEach(error ->
					error.setMessages(errors.stream()
							.filter (fieldError -> fieldError.getField().equals(error.getField()))
							.map    (FieldError::getDefaultMessage)
							.collect(Collectors.toList()))
			);
		}
		return form;
	}
}
