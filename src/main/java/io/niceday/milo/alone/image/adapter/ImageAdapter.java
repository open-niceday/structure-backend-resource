package io.niceday.milo.alone.image.adapter;

import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.milo.alone.image.bridge.ImageBridge;
import io.niceday.milo.alone.image.entity.Image;
import io.niceday.milo.alone.image.service.ImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description image adapter
 **********************************************************************************************************************/
@Component
@Transactional
@RequiredArgsConstructor
public class ImageAdapter {

    private final ImageService imageService;
    private final ImageBridge  imageBridge;

    public Image add(MultipartFile file) {

        if(Objects.isNull(file)){
            throw new NotFoundException();
        }
        return imageService.add(imageBridge.add(file));
    }

    public void remove(String imageId) {

        imageService.remove(imageId);
        imageBridge.remove (imageId);
    }
}
