package io.niceday.milo.alone.notice.controller;

import io.niceday.milo.alone.notice.form.NoticeForm;
import io.niceday.milo.alone.notice.form.NoticeForm.Response;
import io.niceday.milo.alone.notice.predicate.NoticePredicate;
import io.niceday.milo.alone.notice.service.NoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static io.niceday.milo.alone.notice.mapper.NoticeMapper.mapper;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description notice controller
 **********************************************************************************************************************/
@Api(description="공지")
@RestController
@RequestMapping("${property.api.end-point}")
@RequiredArgsConstructor
public class NoticeController {
    
    private final NoticeService noticeService;

    @ApiOperation("목록")
    @GetMapping("/notices/pages")
    public Page<Response.FindAll> getPage(@Valid NoticeForm.Request.Find find, @PageableDefault Pageable pageable) {
        return noticeService.getPage(NoticePredicate.search(find), pageable).map(mapper::toFindAll);
    }
    
    @ApiOperation("조회")
    @GetMapping("/notices/{noticeId}")
    public Response.FindOne get(@PathVariable Long noticeId) {
        return mapper.toFindOne(noticeService.get(noticeId));
    }
    
    @ApiOperation("등록")
    @PostMapping("/notices")
    public Response.FindOne add(@Valid @RequestBody NoticeForm.Request.Add add){
        return mapper.toFindOne(noticeService.add(mapper.toEntity(add)));
    }
    
    @ApiOperation("수정")
    @PutMapping("/notices/{noticeId}")
    public Response.FindOne modify(@PathVariable Long noticeId, @Valid @RequestBody NoticeForm.Request.Modify modify){
        return mapper.toFindOne(noticeService.modify(mapper.toEntity(modify, noticeId), noticeId));
    }
    
    @ApiOperation("삭제")
    @DeleteMapping("/notices/{noticeId}")
    public void remove(@PathVariable Long noticeId){
        noticeService.remove(noticeId);
    }
}
