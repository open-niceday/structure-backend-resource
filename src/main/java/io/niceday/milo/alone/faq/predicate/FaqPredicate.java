package io.niceday.milo.alone.faq.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import io.niceday.milo.alone.faq.entity.QFaq;
import io.niceday.milo.alone.faq.form.FaqForm.Request;

import java.util.Optional;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description faq predicate
 **********************************************************************************************************************/
public class FaqPredicate {

   public static Predicate search(Request.Find find){

       QFaq           faq     = QFaq.faq;
       BooleanBuilder builder = new BooleanBuilder();

       Optional.ofNullable(find.getFaqType()).ifPresent(p -> builder.and(faq.faqType.eq(p)));
       Optional.ofNullable(find.getKeyword()).ifPresent(p -> builder.and(faq.title.startsWith(p).or(faq.creator.name.startsWith(p))));

       return builder;
   }
}
