package io.niceday.milo.alone.faq.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description faq type
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum FaqType {

     ALL      ("전체")
    ,CALCULATE("정산")
    ,SELL     ("판매/프로모션")
    ,SERVICE  ("서비스이용");

    private String description;
}
