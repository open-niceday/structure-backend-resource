-- account(password:1234)
insert into account (id, account_status, email, password, name, created_by, created_at) values (1, 'ENABLE', 'lucas@niceday.io',  '{bcrypt}$2a$10$Czj71JwN8KCX7qN3e4114eirwFonEEX/i6bkclp5js09rXMPuaj/O', '임현우', 1, now());
insert into account (id, account_status, email, password, name, created_by, created_at) values (2, 'ENABLE', 'mushi@niceday.io' , '{bcrypt}$2a$10$Czj71JwN8KCX7qN3e4114eirwFonEEX/i6bkclp5js09rXMPuaj/O', '이상윤', 1, now());
insert into account (id, account_status, email, password, name, created_by, created_at) values (3, 'ENABLE', 'martin@niceday.io', '{bcrypt}$2a$10$Czj71JwN8KCX7qN3e4114eirwFonEEX/i6bkclp5js09rXMPuaj/O', '한라산', 1, now());
insert into account (id, account_status, email, password, name, created_by, created_at) values (4, 'ENABLE', 'henry@niceday.io' , '{bcrypt}$2a$10$Czj71JwN8KCX7qN3e4114eirwFonEEX/i6bkclp5js09rXMPuaj/O', '박정수', 1, now());
insert into account (id, account_status, email, password, name, created_by, created_at) values (5, 'ENABLE', 'van@niceday.io'   , '{bcrypt}$2a$10$Czj71JwN8KCX7qN3e4114eirwFonEEX/i6bkclp5js09rXMPuaj/O', '배상환', 1, now());
insert into account (id, account_status, email, password, name, created_by, created_at) values (6, 'ENABLE', 'carrot@niceday.io', '{bcrypt}$2a$10$Czj71JwN8KCX7qN3e4114eirwFonEEX/i6bkclp5js09rXMPuaj/O', '정병훈', 1, now());


insert into role (id, role_type, description, created_by, created_at) values ( 1, 'ROLE_COMMON',          '공통',             1, now());
insert into role (id, role_type, description, created_by, created_at) values ( 2, 'ROLE_ADMIN_MASTER',    '관리자-마스터',      1, now());
insert into role (id, role_type, description, created_by, created_at) values ( 3, 'ROLE_ADMIN_MANAGER',   '관리자-매니저',      1, now());
insert into role (id, role_type, description, created_by, created_at) values ( 4, 'ROLE_TIC_SUPERVISOR',  '사용자-슈퍼바이저',   1, now());
insert into role (id, role_type, description, created_by, created_at) values ( 5, 'ROLE_TIC_USER',        '사용자-유저',        1, now());
insert into role (id, role_type, description, created_by, created_at) values (10, 'ROLE_ADMIN_HOTEL',     '관리자-숙소관리',     1, now());
insert into role (id, role_type, description, created_by, created_at) values (11, 'ROLE_ADMIN_ACCOUNT',   '관리자-사용자관리',    1, now());
insert into role (id, role_type, description, created_by, created_at) values (12, 'ROLE_ADMIN_PARTNER',   '관리자-파트너지원',    1, now());
insert into role (id, role_type, description, created_by, created_at) values (13, 'ROLE_USER_DASHBOARD',  '사용자-대시보드',     1, now());
insert into role (id, role_type, description, created_by, created_at) values (14, 'ROLE_USER_SELL',       '사용자-판매관리',     1, now());
insert into role (id, role_type, description, created_by, created_at) values (15, 'ROLE_USER_PROMOTION',  '사용자-프로모션',     1, now());
insert into role (id, role_type, description, created_by, created_at) values (16, 'ROLE_USER_RESERVE',    '사용자-예약관리',     1, now());
insert into role (id, role_type, description, created_by, created_at) values (17, 'ROLE_USER_HOTEL',      '사용자-숙소관리',     1, now());
insert into role (id, role_type, description, created_by, created_at) values (18, 'ROLE_USER_PARTNER',    '사용자-파트너지원',    1, now());
insert into role (id, role_type, description, created_by, created_at) values (19, 'ROLE_USER_ROOM_HARD',  '사용자-하드블록관리',   1, now());


insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 1, '/api/**'                                          , 'all'     , 'NONE'  , 'ALL'   ,  'DENY', 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 2, '/api/me'                                          , 'me'      , 'GET'   , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 3, '/api/accounts/pages'                              , 'account' , 'GET'   , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 4, '/api/accounts/{accountId}'                        , 'account' , 'GET'   , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 5, '/api/accounts'                                    , 'account' , 'POST'  , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 6, '/api/accounts/{accountId}'                        , 'account' , 'PUT'   , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 7, '/api/roles'                                       , 'role'    , 'POST'  , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 8, '/api/roles/{roleId}'                              , 'role'    , 'DELETE', 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 9, '/api/roles/{roleId}'                              , 'role'    , 'GET'   , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (10, '/api/roles/pages'                                 , 'role'    , 'GET'   , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (11, '/api/roles/{roleId}'                              , 'role'    , 'PUT'   , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (12, '/api/resources'                                   , 'resource', 'POST'  , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (13, '/api/resources/{resourceId}'                      , 'resource', 'DELETE', 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (14, '/api/resources/{resourceId}'                      , 'resource', 'GET'   , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (15, '/api/resources/pages'                             , 'resource', 'GET'   , 'METHOD',  null  , 1, now());
insert into resource_tic_auth (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (16, '/api/resources/{resourceId}'                      , 'resource', 'PUT'   , 'METHOD',  null  , 1, now());

insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 1, '/api/**'                                         , 'all'             , 'NONE'  , 'ALL'   , 'DENY', 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 2, '/api/menus'                                      , 'menu'            , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 3, '/api/images/pages'                               , 'image'           , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 4, '/api/images/{imageId}'                           , 'image'           , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 5, '/api/images'                                     , 'image'           , 'POST'  , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 6, '/api/images/{imageId}'                           , 'image'           , 'DELETE', 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 7, '/api/attaches/pages'                             , 'attach'          , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 8, '/api/attaches/{imageId}'                         , 'attach'          , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 9, '/api/attaches'                                   , 'attach'          , 'POST'  , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (10, '/api/attaches/{imageId}'                         , 'attach'          , 'DELETE', 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (11, '/api/notices/pages'                              , 'notice'          , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (12, '/api/notices/{noticeId}'                         , 'notice'          , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (13, '/api/notices'                                    , 'notice'          , 'POST'  , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (14, '/api/notices/{noticeId}'                         , 'notice'          , 'PUT'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (15, '/api/notices/{noticeId}'                         , 'notice'          , 'DELETE', 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (16, '/api/faqs/pages'                                 , 'faq'             , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (17, '/api/faqs/{faqId}'                               , 'faq'             , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (18, '/api/faqs'                                       , 'faq'             , 'POST'  , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (19, '/api/faqs/{faqId}'                               , 'faq'             , 'PUT'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (20, '/api/faqs/{faqId}'                               , 'faq'             , 'DELETE', 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (21, '/api/faqs/change-sort'                           , 'faq'             , 'PATCH' , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (22, '/api/questions/pages'                            , 'question'        , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (23, '/api/questions/{questionId}'                     , 'question'        , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (24, '/api/questions'                                  , 'question'        , 'POST'  , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (25, '/api/questions/{questionId}'                     , 'question'        , 'PUT'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (26, '/api/questions/{questionId}'                     , 'question'        , 'DELETE', 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (27, '/api/comments/questions/{questionId}'            , 'comment-question', 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (28, '/api/comments/questions/{questionId}'            , 'comment-question', 'POST'  , 'METHOD', null  , 1, now());
insert into resource_tic_admin (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (29, '/api/comments/{commentId}/questions/{questionId}', 'comment-question', 'DELETE', 'METHOD', null  , 1, now());


insert into resource_tic_user  (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 1, '/api/**'                , 'all'   , 'NONE'  , 'ALL'   , 'DENY', 1, now());
insert into resource_tic_user  (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 3, '/api/images/pages'      , 'image' , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_user  (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 4, '/api/images/{imageId}'  , 'image' , 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_user  (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 5, '/api/images'            , 'image' , 'POST'  , 'METHOD', null  , 1, now());
insert into resource_tic_user  (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 6, '/api/images/{imageId}'  , 'image' , 'DELETE', 'METHOD', null  , 1, now());
insert into resource_tic_user  (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 7, '/api/attaches/pages'    , 'attach', 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_user  (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 8, '/api/attaches/{imageId}', 'attach', 'GET'   , 'METHOD', null  , 1, now());
insert into resource_tic_user  (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values ( 9, '/api/attaches'          , 'attach', 'POST'  , 'METHOD', null  , 1, now());
insert into resource_tic_user  (id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (10, '/api/attaches/{imageId}', 'attach', 'DELETE', 'METHOD', null  , 1, now());


insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values ( 1,   1, '/xxx',   null, 1, now(), '관리자-숙소관리',            '숙소관리');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values ( 2,   2, '/xxx',   1,    1, now(), '관리자-숙소관리-숙소관리',      '숙소관리');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values ( 3,   3, '/xxx',   null, 1, now(), '관리자-사용자관리',           '사용자관리');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values ( 4,   4, '/xxx',   3,    1, now(), '관리자-사용자관리-사용자관리',   '사용자관리');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values ( 5,   5, '/xxx',   3,    1, now(), '관리자-사용자관리-가입문의확인',  '가입문의확인');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values ( 6,   6, '/xxx',   null, 1, now(), '관리자-파트너지원',            '파트너지원');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values ( 7,   7, '/xxx',   6,    1, now(), '관리자-파트너지원-파트너지원',    '파트너지원');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values ( 8,   8, '/xxx',   null, 1, now(), '사용자-대시보드',             '대시보드');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values ( 9,   9, '/xxx',   8,    1, now(), '사용자-대시보드-대시보드',       '대시보드');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (10,  10, '/xxx',   null, 1, now(), '사용자-판매관리',             '판매관리');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (11,  11, '/xxx',   10,   1, now(), '사용자-판매관리-요금및재고',     '요금및재고');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (12,  12, '/xxx',   10,   1, now(), '사용자-판매관리-객실및요금제',    '객실및요금제');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (13,  13, '/xxx',   10,   1, now(), '사용자-판매관리-일괄업데이트',    '일괄업데이트');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (14,  14, '/xxx',   10,   1, now(), '사용자-판매관리-취소규정설정',     '취소규정설정');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (15,  15, '/xxx',   null, 1, now(), '사용자-프로모션',              '프로모션');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (16,  16, '/xxx',   15,   1, now(), '사용자-프로모션-프로모션관리',    '프로모션관리');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (17,  17, '/xxx',   null, 1, now(), '사용자-예약관리',              '예약관리');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (18,  18, '/xxx',   17,   1, now(), '사용자-예약관리-예약목록',       '예약목록');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (19,  19, '/xxx',   null, 1, now(), '사용자-숙소관리',              '숙소관리');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (20,  20, '/xxx',   19,   1, now(), '사용자-숙소관리-숙소정보설정',    '숙소정보설정');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (21,  21, '/xxx',   19,   1, now(), '사용자-숙소관리-시스템설정',     '시스템설정');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (22,  22, '/xxx',   null, 1, now(), '사용자-파트너지원',            '파트너지원');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (23,  23, '/xxx',   22,   1, now(), '사용자-파트너지원-파트너지원',    '파트너지원');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (24,  24, '/xxx',   null, 1, now(), '사용자-하드블록관리',           '하드블록관리');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (25,  25, '/xxx',   24,   1, now(), '사용자-하드블록관리-요금및재고',   '요금및재고');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (26,  26, '/xxx',   25,   1, now(), '사용자-하드블록관리-객실및요금제', '객실및요금제');
insert into menu (id, sort, link, parent_id, created_by, created_at, description, name) values (27,  27, '/xxx',   26,   1, now(), '사용자-하드블록관리-일괄업데이트', '일괄업데이트');
