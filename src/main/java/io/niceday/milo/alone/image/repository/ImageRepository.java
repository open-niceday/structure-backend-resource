package io.niceday.milo.alone.image.repository;

import io.niceday.milo.alone.image.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description image repository
 **********************************************************************************************************************/
public interface ImageRepository extends JpaRepository<Image, String>, QuerydslPredicateExecutor<Image> {

}
