package io.niceday.common.engine.config.integrate.common.handler;

import io.niceday.common.engine.config.integrate.common.exception.RestTemplateException;
import io.niceday.common.engine.exception.common.ExceptionCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

/**   
 * @since       2020.04.01
 * @author      lucas
 * @description integrate error handler
 **********************************************************************************************************************/
@Slf4j
@Component
public class RestTemplateErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        return httpResponse.getStatusCode().is5xxServerError() || httpResponse.getStatusCode().is4xxClientError();
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException {
        throw new RestTemplateException(ExceptionCode.E00100100, httpResponse.getStatusCode());
    }
}