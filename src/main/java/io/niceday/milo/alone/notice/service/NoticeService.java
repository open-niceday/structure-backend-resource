package io.niceday.milo.alone.notice.service;

import com.querydsl.core.types.Predicate;
import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.milo.alone.notice.entity.Notice;
import io.niceday.milo.alone.notice.repository.NoticeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static io.niceday.milo.alone.notice.mapper.NoticeMapper.mapper;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description notice service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class NoticeService {
    
    private final NoticeRepository noticeRepository;

    @Transactional(readOnly=true)
    public Page<Notice> getPage(Predicate predicate, Pageable pageable) {
        return noticeRepository.findAll(predicate, pageable);
    }
    
    @Transactional(readOnly=true)
    public Notice get(Long noticeId) {
        return noticeRepository.findById(noticeId).orElseThrow(NotFoundException::new);
    }
    
    public Notice add(Notice notice) {
        return noticeRepository.save(notice);
    }
    
    public Notice modify(Notice notice, Long noticeId) {
        return mapper.modify(notice, get(noticeId));
    }
    
    public void remove(Long noticeId) {
        noticeRepository.delete(get(noticeId));
    }
}