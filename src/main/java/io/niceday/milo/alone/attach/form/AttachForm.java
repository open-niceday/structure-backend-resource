package io.niceday.milo.alone.attach.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description attach form
 **********************************************************************************************************************/
public class AttachForm {

	public static class Response {

		@Data
		public static class FindAll {

			@ApiModelProperty(value="첨부일련번호")
			private String id;

			@ApiModelProperty(value="원본이름")
			private String name;

			@ApiModelProperty(value="경로")
			private String path;

			@ApiModelProperty(value="크기")
			private Long size;
		}

		@Data
		public static class FindOne {

			@ApiModelProperty(value="첨부일련번호")
			private String id;

			@ApiModelProperty(value="원본이름")
			private String name;

			@ApiModelProperty(value="경로")
			private String path;

			@ApiModelProperty(value="크기")
			private Long size;
		}
	}
}
