package io.niceday.milo.alone.comment.mapper;

import io.niceday.milo.alone.comment.entity.Comment;
import io.niceday.milo.alone.comment.form.CommentForm.Request;
import io.niceday.milo.alone.comment.form.CommentForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description comment mapper
 **********************************************************************************************************************/
@Mapper
public interface CommentMapper {
    
    CommentMapper mapper = Mappers.getMapper(CommentMapper.class);

    Response.FindAll toFindAll(Comment entity);
    Response.FindOne toFindOne(Comment entity);

    Comment toEntity(Request.Add form);
}
