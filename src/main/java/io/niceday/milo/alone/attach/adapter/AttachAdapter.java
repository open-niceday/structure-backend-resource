package io.niceday.milo.alone.attach.adapter;

import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.milo.alone.attach.bridge.AttachBridge;
import io.niceday.milo.alone.attach.entity.Attach;
import io.niceday.milo.alone.attach.service.AttachService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description attach adapter
 **********************************************************************************************************************/
@Component
@Transactional
@RequiredArgsConstructor
public class AttachAdapter {

    private final AttachService attachService;
    private final AttachBridge  attachBridge;

    public Attach add(MultipartFile file) {

        if(Objects.isNull(file)){
            throw new NotFoundException();
        }
        return attachService.add(attachBridge.add(file));
    }

    public void remove(String attachId) {

        attachService.remove(attachId);
        attachBridge.remove (attachId);
    }
}
