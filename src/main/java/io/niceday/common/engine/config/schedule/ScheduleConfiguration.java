package io.niceday.common.engine.config.schedule;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;

/**   
 * @since       2021.02.01
 * @author      lucas
 * @description schedule configuration
 **********************************************************************************************************************/
@Configuration 
@EnableScheduling 
@Profile({"prod", "stage", "dev"})
public class ScheduleConfiguration { 
	
}
