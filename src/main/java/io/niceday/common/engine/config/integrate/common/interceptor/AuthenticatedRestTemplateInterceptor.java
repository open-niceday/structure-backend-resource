package io.niceday.common.engine.config.integrate.common.interceptor;

import io.niceday.common.engine.constant.Constant;
import io.niceday.common.security.principal.helper.PrincipalHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Objects;

/**   
 * @since       2020.04.19
 * @author      lucas
 * @description authenticated rest template interceptor
 **********************************************************************************************************************/
@Component
public class AuthenticatedRestTemplateInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {

        HttpHeaders headers = request.getHeaders();
        OAuth2AuthenticationDetails details = PrincipalHelper.getDetails();
        if(Objects.nonNull(details)){
            headers.add(HttpHeaders.AUTHORIZATION, StringUtils.join(details.getTokenType(), Constant.String.BLANK, details.getTokenValue()));
        }

        return execution.execute(request, body);
    }
}