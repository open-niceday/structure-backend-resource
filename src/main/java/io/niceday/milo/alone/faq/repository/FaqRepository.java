package io.niceday.milo.alone.faq.repository;

import com.querydsl.core.types.Predicate;
import io.niceday.milo.alone.faq.entity.Faq;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description faq repository
 **********************************************************************************************************************/
@Repository
public interface FaqRepository extends JpaRepository<Faq, Long>, QuerydslPredicateExecutor<Faq> {

    @EntityGraph(attributePaths={"creator", "attach"})
    Page<Faq> findAll(Predicate predicate, Pageable pageable);
}
