create table oauth_client_details (
     client_id                varchar( 50)
    ,resource_ids             varchar( 50)
    ,client_secret            varchar(100)
    ,scope                    varchar( 30)
    ,authorized_grant_types   varchar( 50)
    ,access_token_validity    int
    ,refresh_token_validity   int
    ,web_server_redirect_uri  varchar( 100)
    ,authorities              varchar( 100)
    ,autoapprove              varchar( 100)
    ,additional_information   varchar(4096)
    ,primary key(client_id)
);

create table oauth_access_token (
     authentication_id  varchar(100) not null unique
    ,token_id           varchar(100)
    ,token              blob
    ,user_name          varchar(2000)
    ,client_id          varchar(100)
    ,authentication     blob
    ,refresh_token      varchar(100)
    ,expire_at          datetime
    ,primary key(token_id)
);

create table oauth_refresh_token (
     token_id       varchar(100)
    ,token          blob
    ,authentication blob
    ,expire_at      datetime
    ,primary key(token_id)
);
