package io.niceday.milo.alone.notice.form;


import io.niceday.common.base.form.BaseForm;
import io.niceday.milo.alone.notice.enumerate.NoticeType;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @since       2021.11.30
 * @author      lucas
 * @description notice form
 **********************************************************************************************************************/
public class NoticeForm {

    public static class Request {

        @Setter
        @Getter
        @ToString
        @Builder
        @AllArgsConstructor
        @NoArgsConstructor
        public static class Find {

            @ApiModelProperty(value="공지유형")
            private NoticeType noticeType;

            @ApiModelProperty(value="제목/작성자")
            private String keyword;
        }

        @Setter
        @Getter
        @ToString
        @Builder(toBuilder=true)
        @AllArgsConstructor
        @NoArgsConstructor
        public static class Add {

            @ApiModelProperty(value="공지유형", required=true)
            @NotNull
            private NoticeType noticeType;

            @ApiModelProperty(value="제목", required=true)
            @NotBlank
            private String title;

            @ApiModelProperty(value="내용", required=true)
            @NotBlank
            private String content;

            @ApiModelProperty(value="첨부")
            @Valid
            private BaseForm.Request.Attach attach;
        }

        @Setter
        @Getter
        @ToString
        @Builder(toBuilder=true)
        @AllArgsConstructor
        @NoArgsConstructor
        public static class Modify {

            @ApiModelProperty(value="공지유형", required=true)
            @NotNull
            private NoticeType noticeType;

            @ApiModelProperty(value="제목", required=true)
            @NotBlank
            private String title;

            @ApiModelProperty(value="내용", required=true)
            @NotBlank
            private String content;

            @ApiModelProperty(value="첨부")
            @Valid
            private BaseForm.Request.Attach attach;
        }
    }

    public static class Response {

        @Data
        public static class FindAll {

            @ApiModelProperty(value="공지일련번호")
            private Long id;

            @ApiModelProperty(value="공지유형")
            private NoticeType noticeType;

            @ApiModelProperty(value="제목")
            private String title;

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;
        }

        @Data
        public static class FindOne {

            @ApiModelProperty(value="공지일련번호")
            private Long id;

            @ApiModelProperty(value="공지유형")
            private NoticeType noticeType;

            @ApiModelProperty(value="제목")
            private String title;

            @ApiModelProperty(value="내용")
            private String content;

            @ApiModelProperty(value="등록일시")
            private LocalDateTime createdAt;

            @ApiModelProperty(value="생성자")
            private BaseForm.Response.Account creator;

            @ApiModelProperty(value="첨부")
            private BaseForm.Response.Attach attach;
        }
    }
}
