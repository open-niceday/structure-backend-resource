package io.niceday.milo.access.account.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.milo.access.account.enumerate.AccountStatus;
import io.niceday.milo.access.role.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * @since       2021.03.10
 * @author      lucas
 * @description account
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("사용자")
public class Account extends Base {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Description("사용자일련번호")
    @Column(nullable=false)
    private Long id;

    @Description("사용자상태")
    @Enumerated(EnumType.STRING)
    @Column(nullable=false, length=30)
    private AccountStatus accountStatus;

    @Description("비밀번호")
    @Column(length=100, updatable=false)
    private String password;

    @Description("이름")
    @Column(nullable=false, length=100)
    private String name;

    @Description("이메일")
    @Column(nullable=false, length=100)
    private String email;

    @Description("권한목록")
    @OneToMany(fetch=FetchType.LAZY)
    @JoinTable(name="account_role", joinColumns=@JoinColumn(name="account_id"), inverseJoinColumns=@JoinColumn(name="role_id"))
    private List<Role> roles;
}
