package io.niceday.milo.access.account;

import io.niceday.common.engine.helper.oauth.adapter.AuthorizationAdapter;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2021.03.19
 * @author      lucas
 * @description accounts
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum Accounts {

     LUCAS (1L,"임현우","1234", "lucas@niceday.io" ){ @Override public String getAuthorization(){ return AuthorizationAdapter.getAuthorization(getEmail(), getPassword()); }}
    ,MUSHI (2L,"이상윤","1234", "mushi@niceday.io" ){ @Override public String getAuthorization(){ return AuthorizationAdapter.getAuthorization(getEmail(), getPassword()); }}
    ,MARTIN(3L,"한라산","1234", "martin@niceday.io"){ @Override public String getAuthorization(){ return AuthorizationAdapter.getAuthorization(getEmail(), getPassword()); }}
    ,HENRY (4L,"박정수","1234", "henry@niceday.io" ){ @Override public String getAuthorization(){ return AuthorizationAdapter.getAuthorization(getEmail(), getPassword()); }}
    ,VAN   (5L,"배상환","1234", "van@niceday.io"   ){ @Override public String getAuthorization(){ return AuthorizationAdapter.getAuthorization(getEmail(), getPassword()); }}
    ,CARROT(6L,"정병훈","1234", "carrot@niceday.io"){ @Override public String getAuthorization(){ return AuthorizationAdapter.getAuthorization(getEmail(), getPassword()); }};

    private Long   id;
    private String name;
    private String password;
    private String email;

    public abstract String getAuthorization();
}