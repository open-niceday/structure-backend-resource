package io.niceday.milo.alone.image.service;

import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.milo.alone.image.entity.Image;
import io.niceday.milo.alone.image.repository.ImageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description image service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class ImageService {

	private final ImageRepository imageRepository;

	@Transactional(readOnly=true)
	public Page<Image> getPage(Pageable pageable) {
		return imageRepository.findAll(pageable);
	}

	@Transactional(readOnly=true)
	public Image get(String imageId) {
		return imageRepository.findById(imageId).orElseThrow(NotFoundException::new);
	}

	@Transactional(readOnly=true)
	public boolean exists(String imageId) {
		return imageRepository.existsById(imageId);
	}

	public Image add(Image image) {
		return imageRepository.save(image);
	}

	public void remove(String imageId) {
		imageRepository.delete(get(imageId));
	}
}
