package io.niceday.common.engine.config.redis;//package io.niceday.common.engine.config.redis;
//
//import com.fasterxml.jackson.annotation.JsonTypeInfo;
//import com.fasterxml.jackson.core.JsonGenerator;
//import com.fasterxml.jackson.databind.DeserializationFeature;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
//import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.listener.ChannelTopic;
//import org.springframework.data.redis.listener.RedisMessageListenerContainer;
//import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
//import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
//import org.springframework.data.redis.serializer.StringRedisSerializer;
//
///**
// * @since       2021.02.09
// * @author      lucas
// * @description redis configuration
// **********************************************************************************************************************/
//@Configuration
//public class RedisConfiguration {
//
//    @Autowired
//    private RedisSubscriber redisSubscriber;
//
//    @Bean
//    public RedisConnectionFactory redisConnectionFactory() {
//        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
//        redisStandaloneConfiguration.setHostName("localhost");
//        redisStandaloneConfiguration.setPort(6379);
//        //redisStandaloneConfiguration.setPassword(redisPwd);
//        LettuceConnectionFactory lettuceConnectionFactory = new LettuceConnectionFactory(redisStandaloneConfiguration);
//        return lettuceConnectionFactory;
//    }
//
//    @Bean
//    public GenericJackson2JsonRedisSerializer genericJackson2JsonRedisSerializer() {
//        ObjectMapper objectMapper = new ObjectMapper ();
//        objectMapper.enable (JsonGenerator.Feature.IGNORE_UNKNOWN);
//        objectMapper.disable (DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
//        objectMapper.enableDefaultTyping (ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
//        GenericJackson2JsonRedisSerializer jsonRedisSerializer = new GenericJackson2JsonRedisSerializer (objectMapper);
//        return jsonRedisSerializer;
//    }
//
//    @Bean
//    public RedisTemplate<String, Event> redisTemplate() {
//        RedisTemplate<String, Event> redisTemplate = new RedisTemplate<>();
//        redisTemplate.setConnectionFactory(redisConnectionFactory());
//        redisTemplate.setKeySerializer(new StringRedisSerializer());
//        //redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
//        //redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(Event.class));
//        redisTemplate.setValueSerializer(genericJackson2JsonRedisSerializer());
//        return redisTemplate;
//    }
//
//    @Bean
//    public ChannelTopic topic() {
//        return new ChannelTopic("test-broker");
//    }
//}
