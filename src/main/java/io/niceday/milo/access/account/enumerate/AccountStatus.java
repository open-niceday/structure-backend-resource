package io.niceday.milo.access.account.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2021.03.19
 * @author      lucas
 * @description account status
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum AccountStatus {

     ENABLE ("활성")
    ,DISABLE("비활성");

    private final String description;
}
