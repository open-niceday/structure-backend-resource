package io.niceday.milo.alone.comment.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.milo.alone.question.entity.Question;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description comment
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("댓글")
public class Comment extends Base {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Description("댓글일련번호")
	@Column(nullable=false)
	private Long id;

	@Description("내용")
	@Column(nullable=false, length=60000)
	private String content;


	@Description("문의")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinTable(name="comment_question", joinColumns=@JoinColumn(name="comment_id"), inverseJoinColumns=@JoinColumn(name="question_id"))
	private Question question;
}