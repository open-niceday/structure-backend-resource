package io.niceday.milo.alone.image;

import io.niceday.common.engine.test.TestHelper;
import io.niceday.milo.alone.image.form.ImageForm.Response;
import lombok.SneakyThrows;

import static io.niceday.common.engine.helper.model.ObjectHelper.toInstance;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @since       2021.05.24
 * @author      lob
 * @description image helper
 **********************************************************************************************************************/
public class ImageHelper extends TestHelper {

	@SneakyThrows
	public static Response.FindOne add(String path) {
		return toInstance(Response.FindOne.class,
				mock.perform(file("/api/images", path))
						.andDo(print())
						.andExpect(status().isOk())
		);
	}

	@SneakyThrows
	public static void remove(String id) {
		mock.perform(delete("/api/images/{imageId}", id))
				.andDo(print())
				.andExpect(status().isOk());
	}
}
