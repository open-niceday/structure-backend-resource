package io.niceday.milo.alone.notice.mapper;

import io.niceday.milo.alone.notice.entity.Notice;
import io.niceday.milo.alone.notice.form.NoticeForm.Request;
import io.niceday.milo.alone.notice.form.NoticeForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description notice mapper
 **********************************************************************************************************************/
@Mapper
public interface NoticeMapper {
    
    NoticeMapper mapper = Mappers.getMapper(NoticeMapper.class);
    
    Response.FindAll toFindAll(Notice entity);
    Response.FindOne toFindOne(Notice entity);

    Notice toEntity(Request.Add    form);
    Notice toEntity(Request.Modify form, Long id);
    
    @Mapping(target="id", ignore=true)
    Notice modify(Notice source, @MappingTarget Notice target);
}
