package io.niceday.milo.alone.attach.bridge;

import io.niceday.common.engine.config.integrate.aws.s3.S3Content;
import io.niceday.common.engine.config.integrate.aws.s3.S3Integration;
import io.niceday.common.engine.config.integrate.aws.s3.S3Type;
import io.niceday.milo.alone.attach.entity.Attach;
import io.niceday.milo.alone.attach.service.AttachService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description attach bridge
 **********************************************************************************************************************/
@Component
@RequiredArgsConstructor
public class AttachBridge {

    private final AttachService attachService;

    public Attach add(MultipartFile file) {
        S3Content content = S3Integration.add(S3Type.ATTACH, file);
        return Attach.builder()
                .id  (content.getId())
                .name(content.getName())
                .path(content.getPath())
                .size(content.getSize())
                .build();
    }

    public void remove(String attachId) {
        Attach attach = attachService.get(attachId);
        S3Integration.remove(attach.getPath());
    }
}
