package io.niceday.milo.alone.question.service;

import com.querydsl.core.types.Predicate;
import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.milo.alone.question.entity.Question;
import io.niceday.milo.alone.question.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static io.niceday.milo.alone.question.mapper.QuestionMapper.mapper;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description question service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class QuestionService {
    
    private final QuestionRepository questionRepository;

    @Transactional(readOnly=true)
    public Page<Question> getPage(Predicate predicate, Pageable pageable) {
        return questionRepository.findAll(predicate, pageable);
    }
    
    @Transactional(readOnly=true)
    public Question get(Long questionId) {
        return questionRepository.findById(questionId).orElseThrow(NotFoundException::new);
    }
    
    public Question add(Question question) {
        return questionRepository.save(question);
    }
    
    public Question modify(Question question, Long questionId) {
        return mapper.modify(question, get(questionId));
    }
    
    public void remove(Long questionId) {
        questionRepository.delete(get(questionId));
    }
}