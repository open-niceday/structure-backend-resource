package io.niceday.milo.access.account.service;

import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.milo.access.account.entity.Account;
import io.niceday.milo.access.account.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @since       2022.05.02
 * @author      pollra
 * @description account service
 **********************************************************************************************************************/
@Component
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public Account get(Long accountId) {
        return accountRepository.findById(accountId).orElseThrow(NotFoundException::new);
    }
}
