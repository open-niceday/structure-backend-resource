-- sample
create table sample (
     id          bigint not null auto_increment
    ,user_id     varchar(50)
    ,content     varchar(1000)
    ,created_by  bigint
    ,created_at  datetime
    ,updated_by  bigint
    ,updated_at  datetime
    ,primary key(id)
);