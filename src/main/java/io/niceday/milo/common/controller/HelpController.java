package io.niceday.milo.common.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description help controller
 **********************************************************************************************************************/
@Api(description="도움말", tags="_help-controller")
@RestController
@RequestMapping("${property.api.end-point}")
@Profile({"default", "test", "dev"})
public class HelpController {

    @ApiOperation("공통 에러 코드")
    @GetMapping  ("/helps/status-codes")
    @ApiResponses({
         @ApiResponse(code=400, response=ErrorForm.Error400.class, message="[비니지스]사용자 입력 유효성 검증 오류")
        ,@ApiResponse(code=401, response=ErrorForm.Error401.class, message="[인증]사용자 토큰 오류")
        ,@ApiResponse(code=403, response=ErrorForm.Error403.class, message="[인증]사용자 권한 오류")
        ,@ApiResponse(code=404, response=ErrorForm.Error404.class, message="[비니지스]존재하지 않은 데이터 조회 오류")
        ,@ApiResponse(code=406, response=ErrorForm.Error406.class, message="[비니지스]잘못 된 접근 오류")
        ,@ApiResponse(code=503, response=ErrorForm.Error503.class, message="[연동]연동 오류")
    })
    public String statusCodes() {
        return StringUtils.EMPTY;
    }
}