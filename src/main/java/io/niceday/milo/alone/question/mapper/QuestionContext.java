package io.niceday.milo.alone.question.mapper;

import io.niceday.milo.alone.question.entity.Question;
import lombok.Builder;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeforeMapping;
import org.mapstruct.MappingTarget;

/**
 * @since       2022.12.01
 * @author      lucas
 * @description question context
 **********************************************************************************************************************/
@Builder
public class QuestionContext {

    private Question question;

    @BeforeMapping
    public void init(@MappingTarget Question target) {
        this.question = target;
    }

    @AfterMapping
    public void set(@MappingTarget Question target) {
        target.setAnswerYn(Boolean.FALSE);
    }

    public static QuestionContext create() {
        return QuestionContext.builder().build();
    }
}
