package io.niceday.common.base.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2022.10.20
 * @author      minam
 * @description service section type
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum ServiceSectionType {

     LIVE           ("LIVE")
    ,OFFLINE_CONCERT("오프라인공연");

    private final String description;
}
