package io.niceday.milo.alone.comment.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import io.niceday.milo.alone.comment.entity.QComment;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description comment predicate
 **********************************************************************************************************************/
public class CommentPredicate {

   public static Predicate question(Long questionId){

       QComment       comment = QComment.comment;
       BooleanBuilder builder = new BooleanBuilder();

       return builder.and(comment.question.id.eq(questionId));
   }
}
