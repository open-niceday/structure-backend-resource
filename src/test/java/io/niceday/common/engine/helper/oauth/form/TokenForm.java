package io.niceday.common.engine.helper.oauth.form;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @since       2020.03.11
 * @author      lucas
 * @description token form
 **********************************************************************************************************************/
public class TokenForm {

    @Getter
    @Setter
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Request {

        @JsonProperty("grant_type")
        private String grantType;

        @JsonProperty("client_id")
        private String clientId;

        @JsonProperty("login_type")
        private String loginType;
        private String username;
        private String password;
    }

    @Data
    public static class Response {

        @JsonProperty("access_token")
        private String accessToken;

        @JsonProperty("token_type")
        private String tokenType;

        @JsonProperty("refresh_token")
        private String refreshToken;

        @JsonProperty("scope")
        private String scope;

        @JsonProperty("expires_in")
        private Long   expiresIn;
    }
}