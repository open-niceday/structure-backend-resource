package io.niceday.milo.alone.faq.service;

import com.querydsl.core.types.Predicate;
import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.milo.alone.faq.entity.Faq;
import io.niceday.milo.alone.faq.repository.FaqRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static io.niceday.milo.alone.faq.mapper.FaqMapper.mapper;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description faq service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class FaqService {
    
    private final FaqRepository faqRepository;

    @Transactional(readOnly=true)
    public Page<Faq> getPage(Predicate predicate, Pageable pageable) {
        return faqRepository.findAll(predicate, pageable);
    }
    
    @Transactional(readOnly=true)
    public Faq get(Long faqId) {
        return faqRepository.findById(faqId).orElseThrow(NotFoundException::new);
    }
    
    public Faq add(Faq faq) {
        return faqRepository.save(faq);
    }
    
    public Faq modify(Faq faq, Long faqId) {
        return mapper.modify(faq, get(faqId));
    }
    
    public void remove(Long faqId) {
        faqRepository.delete(get(faqId));
    }

    public Faq changeSort(Long sourceId, Long targetId) {

        Faq  sourceFaq  = get(sourceId);
        Faq  targetFaq  = get(targetId);
        Long sourceSort = sourceFaq.getSort();
        Long targetSort = targetFaq.getSort();

        sourceFaq.setSort(Optional.ofNullable(targetSort).orElse(targetId));
        targetFaq.setSort(Optional.ofNullable(sourceSort).orElse(sourceId));

        return sourceFaq;
    }
}