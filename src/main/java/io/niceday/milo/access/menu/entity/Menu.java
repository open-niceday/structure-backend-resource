package io.niceday.milo.access.menu.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.milo.access.role.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import java.util.List;

/**
 * @since       2022.04.11
 * @author      tony
 * @description menu
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("메뉴")
public class Menu extends Base {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Description("메뉴일련번호")
    @Column(nullable=false)
    private Long id;

    @Description("이름")
    @Column(nullable=false, length=200)
    private String name;

    @Description("순서")
    @Column(nullable=false)
    private Integer sort;

    @Description("링크")
    @Column(nullable=false, length=500)
    private String link;

    @Description("상위-메뉴")
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="parent_id")
    private Menu parent;

    @Description("하위-메뉴")
    @OneToMany(mappedBy="parent", fetch=FetchType.LAZY)
    @OrderBy(value = "sort asc")
    private List<Menu> children;

    @Description("권한목록")
    @ManyToMany(mappedBy="menus", fetch=FetchType.LAZY)
    private List<Role> roles;
}
