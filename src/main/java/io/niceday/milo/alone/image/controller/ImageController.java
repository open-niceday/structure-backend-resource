package io.niceday.milo.alone.image.controller;

import io.niceday.milo.alone.image.adapter.ImageAdapter;
import io.niceday.milo.alone.image.form.ImageForm.Response;
import io.niceday.milo.alone.image.service.ImageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import static io.niceday.milo.alone.image.mapper.ImageMapper.mapper;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description image controller
 **********************************************************************************************************************/
@Api(description="이미지")
@RestController
@RequiredArgsConstructor
@RequestMapping("${property.api.end-point}")
public class ImageController {

	private final ImageService imageService;
	private final ImageAdapter imageAdapter;

	@Deprecated
	@ApiOperation("목록")
	@GetMapping("/images/pages")
	public Page<Response.FindAll> getPage(@PageableDefault Pageable pageable) {
		return imageService.getPage(pageable).map(mapper::toFindAll);
	}

	@Deprecated
	@ApiOperation("조회")
	@GetMapping("/images/{imageId}")
	public Response.FindOne get(@PathVariable String imageId) {
		return mapper.toFindOne(imageService.get(imageId));
	}

	@ApiOperation("등록")
	@PostMapping("/images")
	public Response.FindOne add(MultipartFile file) {
		return mapper.toFindOne(imageAdapter.add(file));
	}

	@Deprecated
	@ApiOperation("삭제")
	@DeleteMapping("/images/{imageId}")
	public void remove(@PathVariable String imageId) {
		imageAdapter.remove(imageId);
	}
}
