package io.niceday.milo.alone.image.validator;

import io.niceday.milo.alone.image.service.ImageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @since       2022.12.01
 * @author      lucas
 * @description image validator
 **********************************************************************************************************************/
@Component
public class ImageValidator {

    private static ImageService imageService;

    @Autowired
    private ImageValidator(ImageService imageService) {
        ImageValidator.imageService = imageService;
    }

    public static boolean isValid(String imageId) {
        return StringUtils.isBlank(imageId) ? Boolean.TRUE : imageService.exists(imageId);
    }
}
