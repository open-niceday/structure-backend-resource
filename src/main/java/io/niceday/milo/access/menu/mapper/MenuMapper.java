package io.niceday.milo.access.menu.mapper;

import io.niceday.milo.access.menu.entity.Menu;
import io.niceday.milo.access.menu.form.MenuForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @since       2022.06.02
 * @author      mushi
 * @description menu mapper
 **********************************************************************************************************************/
@Mapper
public interface MenuMapper {

    MenuMapper mapper = Mappers.getMapper(MenuMapper.class);

    List<Response.FindOne> toFindOne(List<Menu> entities);
}
