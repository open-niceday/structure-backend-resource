package io.niceday.milo.alone.image;

import io.niceday.common.engine.test.SuperTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import static io.niceday.milo.alone.image.ImageHelper.add;
import static io.niceday.milo.alone.image.ImageHelper.remove;

/**
 * @since       2021.05.24
 * @author      lob
 * @description image test
 *               - https://statics.stayg.tv/u-images/2021/6/14/268b0a27-7326-4a5f-96c0-1c950a3e39dd
 **********************************************************************************************************************/
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
@DisplayName("이미지")
public class ImageTest extends SuperTest {

	@Test
	@DisplayName("등록")
	public void t01_add() {
		add("/static/images/img1.png");
	}

	@Test
	@DisplayName("삭제")
	public void t02_remove() {
		remove(add("/static/images/img1.png").getId());
	}
}
