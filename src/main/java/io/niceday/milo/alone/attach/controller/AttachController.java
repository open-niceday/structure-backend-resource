package io.niceday.milo.alone.attach.controller;

import io.niceday.milo.alone.attach.adapter.AttachAdapter;
import io.niceday.milo.alone.attach.form.AttachForm.Response;
import io.niceday.milo.alone.attach.service.AttachService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import static io.niceday.milo.alone.attach.mapper.AttachMapper.mapper;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description attach controller
 **********************************************************************************************************************/
@Api(description="첨부")
@RestController
@RequiredArgsConstructor
@RequestMapping("${property.api.end-point}")
public class AttachController {

	private final AttachService attachService;
	private final AttachAdapter attachAdapter;

	@Deprecated
	@ApiOperation("목록")
	@GetMapping("/attaches/pages")
	public Page<Response.FindAll> getPage(@PageableDefault Pageable pageable) {
		return attachService.getPage(pageable).map(mapper::toFindAll);
	}

	@Deprecated
	@ApiOperation("조회")
	@GetMapping("/attaches/{attachId}")
	public Response.FindOne get(@PathVariable String attachId) {
		return mapper.toFindOne(attachService.get(attachId));
	}

	@ApiOperation("등록")
	@PostMapping("/attaches")
	public Response.FindOne add(MultipartFile file) {
		return mapper.toFindOne(attachAdapter.add(file));
	}

	@Deprecated
	@ApiOperation("삭제")
	@DeleteMapping("/attaches/{attachId}")
	public void remove(@PathVariable String attachId) {
		attachAdapter.remove(attachId);
	}
}
