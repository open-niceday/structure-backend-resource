package io.niceday.milo.access.menu.controller;

import io.niceday.common.security.principal.helper.PrincipalHelper;
import io.niceday.milo.access.menu.form.MenuForm.Response;
import io.niceday.milo.access.menu.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static io.niceday.milo.access.menu.mapper.MenuMapper.mapper;


/**
 * @since       2022.06.02
 * @author      mushi
 * @description menu controller
 **********************************************************************************************************************/
@Api(description="메뉴")
@RestController
@RequiredArgsConstructor
@RequestMapping("${property.api.end-point}")
public class MenuController {

    private final MenuService menuService;

    @ApiOperation("목록")
    @GetMapping("/menus")
    @ApiResponses({
            @ApiResponse(code=401, message="[E00010006]인증오류가 발생하였습니다."),
            @ApiResponse(code=403, message="[E00010007]권한이 불충분합니다.")
    })
    public List<Response.FindOne> getMenus() {
        return mapper.toFindOne(menuService.getMenus(PrincipalHelper.getAccount().getRoles()));
    }
}
