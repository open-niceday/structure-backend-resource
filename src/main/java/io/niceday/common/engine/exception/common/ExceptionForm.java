package io.niceday.common.engine.exception.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @since       2020.20.24
 * @author      lucas
 * @description exception form
 **********************************************************************************************************************/
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionForm {

	private ExceptionCode code;
	private String        message;


	public static ExceptionForm create(ExceptionCode code){
		return ExceptionForm.builder()
				.code   (code)
				.message(null)
				.build();
	}

	public static ExceptionForm create(ExceptionCode code, String message){
		return ExceptionForm.builder()
				.code   (code)
				.message(message)
				.build();
	}
}