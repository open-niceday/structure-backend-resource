package io.niceday.common.engine.config.integrate.aws.s3;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.util.StringUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.UUID;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description s3 integration
 **********************************************************************************************************************/
@Slf4j
@Service
public class S3Integration {

    private static AmazonS3 s3;
    private static String 	bucket;
    private static Path     location;

	@Autowired
	@SneakyThrows
	private S3Integration( @Value("${cloud.aws.s3.bucket}"              ) String bucket
					      ,@Value("${cloud.aws.region.static}"          ) String region
					      ,@Value("${cloud.aws.credentials.access-key}" ) String access
					      ,@Value("${cloud.aws.credentials.secret-key}" ) String secret
					      ,@Value("${spring.servlet.multipart.location}") String location) {
        Path locationPath = Paths.get(location);
        FileUtils.forceMkdir    (locationPath.toFile());
		FileUtils.cleanDirectory(locationPath.toFile());

        S3Integration.bucket   = bucket;
        S3Integration.location = locationPath;
        S3Integration.s3       = AmazonS3ClientBuilder.standard()
				.withRegion     (region)
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(access, secret)))
				.build();
	}

	@SneakyThrows
	public static S3Content add(S3Type type, MultipartFile file) {

		String id   = getId();
		String path = getPath(type, id);
		File   temp = location.resolve(id).toFile();

		file.transferTo(temp);
		S3Content content = S3Content.builder()
				.id  (id)
				.path(getExtensionWithDot(path, file))
				.size(temp.length())
				.name(file.getOriginalFilename())
				.build();

		transferAndRemoveFile(getExtensionWithDot(path, file), file.getContentType(), temp);
		return content;
	}

	public static void remove(String path) {
		s3.deleteObject(getDeleteObject(path));
	}


	private static void transferAndRemoveFile(String path, String contentType, File file){
		s3.putObject(getPutObject(path, contentType, file));
		FileUtils.deleteQuietly(file);
	}

	private static PutObjectRequest getPutObject(String path, String contentType, File file){
		PutObjectRequest request = new PutObjectRequest(bucket, path, file).withCannedAcl(CannedAccessControlList.PublicRead);
		ObjectMetadata   meta    = new ObjectMetadata();
		meta.setContentType(contentType);
		request.setMetadata(meta);
		return request;
	}

	private static DeleteObjectRequest getDeleteObject(String path){
		return new DeleteObjectRequest(bucket, path);
	}

	private static String getId(){
		return UUID.randomUUID().toString();
	}

	private static String getPath(S3Type type, String fileName){
		return Paths.get(StringUtils.lowerCase(type.name()))
				.resolve(String.valueOf(LocalDate.now().getYear()))
				.resolve(String.valueOf(LocalDate.now().getMonthValue()))
				.resolve(String.valueOf(LocalDate.now().getDayOfMonth()))
				.resolve(fileName)
				.toString();
	}

	private static String getExtensionWithDot(String fileName, MultipartFile file){
		return fileName + FilenameUtils.EXTENSION_SEPARATOR + FilenameUtils.getExtension(file.getOriginalFilename());
	}
}
