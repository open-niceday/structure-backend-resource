package io.niceday.milo.alone.attach.repository;

import io.niceday.milo.alone.attach.entity.Attach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description attach repository
 **********************************************************************************************************************/
public interface AttachRepository extends JpaRepository<Attach, String>, QuerydslPredicateExecutor<Attach> {

}
