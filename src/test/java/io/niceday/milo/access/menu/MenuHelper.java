package io.niceday.milo.access.menu;

import io.niceday.common.engine.test.TestHelper;
import lombok.SneakyThrows;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @since       2022.11.29
 * @author      martin
 * @description menu helper
 **********************************************************************************************************************/
public class MenuHelper extends TestHelper {

	@SneakyThrows
	public static void getMenus() {
		mock.perform(get  ("/api/menus"))
				.andDo    (print())
				.andExpect(status().isOk());
	}
}
