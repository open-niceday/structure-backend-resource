package io.niceday;

import io.niceday.common.engine.config.bean.DefaultBeanNameGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @since       2021.02.01
 * @author      lucas
 * @description backend application
 **********************************************************************************************************************/
@SpringBootApplication
@ComponentScan(nameGenerator=DefaultBeanNameGenerator.class)
public class BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}
}
