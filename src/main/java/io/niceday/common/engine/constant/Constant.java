package io.niceday.common.engine.constant;

/**
* @since       2021.03.10
* @author      lucas
* @description constant
**********************************************************************************************************************/
public class Constant {

    public static class String {
        public final static java.lang.String EMPTY     = "";
        public final static java.lang.String BLANK     = " ";
        public final static java.lang.String ASTERISK  = "*";
        public final static java.lang.String COMMA     = ",";
        public final static java.lang.String DOT       = ".";
        public final static java.lang.String SLASH     = "/";
        public final static java.lang.String BACKSLASH = "\\";
        public final static java.lang.String PIPE      = "|";
        public final static java.lang.String COLON     = ":";
        public final static java.lang.String QUESTION  = "?";
        public final static java.lang.String DASH      = "-";
    }

    public static class Integer {
        public final static java.lang.Integer MINUS_ONE =  -1;
        public final static java.lang.Integer ZERO      =  0;
        public final static java.lang.Integer ONE       =  1;
        public final static java.lang.Integer TWO       =  2;
        public final static java.lang.Integer THREE     =  3;
        public final static java.lang.Integer TEN       = 10;
        public final static java.lang.Integer TWELVE    = 12;
        public final static java.lang.Integer FIFTEEN   = 15;
        public final static java.lang.Integer THIRTY    = 30;
        public final static java.lang.Integer ONE_THOUSAND = 1000;
    }

    public static class Long {
        public final static java.lang.Long ZERO    =  0L;
        public final static java.lang.Long ONE     =  1L;
        public final static java.lang.Long TWO     =  2L;
        public final static java.lang.Long THREE   =  3L;
        public final static java.lang.Long TEN     = 10L;
        public final static java.lang.Long FIFTEEN = 15L;
        public final static java.lang.Long THIRTY  = 30L;
    }

    public static class Test {
        public final static java.lang.String IMAGE_ID = "268b0a27-7326-4a5f-96c0-1c950a3e39dd";
    }

    public static class Regex {
        public final static java.lang.String CONSTRAINT_VERSION  = "^[0-9]{1,6}$";
        public final static java.lang.String COLOR_CODE          = "^(?:#|0x)?([A-Fa-f\\d]{8}|[A-Fa-f\\d]{6}|[A-Fa-f\\d]{3})$";
        public final static java.lang.String NUMBER_AND_ALPHABET = "^[a-zA-Z0-9]+$";
    }

    public static class Example {
        public final static java.lang.String COLOR  = "#999999";
    }
}