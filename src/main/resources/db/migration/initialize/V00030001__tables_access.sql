create table account (
     id             bigint       not null auto_increment
    ,account_status varchar( 30) not null
    ,name           varchar(100) not null
    ,email          varchar(100) not null
    ,password       varchar(100)
    ,created_by     bigint
    ,created_at     datetime
    ,updated_by     bigint
    ,updated_at     datetime
    ,primary key(id)
) auto_increment = 10000;

create table role (
     id           bigint       not null auto_increment
    ,role_type    varchar( 30) not null
    ,description  varchar(500)
    ,created_by   bigint       not null
    ,created_at   datetime     not null
    ,updated_by   bigint
    ,updated_at   datetime
    ,primary key(id)
) auto_increment = 10000;

create table menu (
    id          bigint       not null auto_increment,
    name        varchar(200) not null,
    sort        bigint       not null,
    link        varchar(500) not null,
    description varchar(500) not null,
    parent_id   bigint,
    created_by  bigint       not null,
    created_at  datetime     not null,
    updated_by  bigint,
    updated_at  datetime,
    primary key (id)
) auto_increment = 10000;

create table account_role (
     account_id bigint not null
    ,role_id    bigint not null
    ,primary key(account_id, role_id)
    ,foreign key(account_id) references account(id)
    ,foreign key(role_id)    references role(id)
);

create table role_menu
(
    role_id bigint not null,
    menu_id bigint not null,
    primary key (role_id, menu_id),
    foreign key (role_id) references role (id),
    foreign key (menu_id) references menu (id)
);

create table resource_tic_auth (
     id              bigint       not null auto_increment
    ,name            varchar(300) not null
    ,method_type     varchar( 30) not null
    ,scope_type      varchar( 30) not null
    ,permission_type varchar( 30)
    ,description     varchar(500)
    ,created_by      bigint       not null
    ,created_at      datetime     not null
    ,updated_by      bigint
    ,updated_at      datetime
    ,primary key(id)
) auto_increment = 10000;

create table resource_tic_admin (
     id              bigint       not null auto_increment
    ,name            varchar(300) not null
    ,method_type     varchar( 30) not null
    ,scope_type      varchar( 30) not null
    ,permission_type varchar( 30)
    ,description     varchar(500)
    ,created_by      bigint       not null
    ,created_at      datetime     not null
    ,updated_by      bigint
    ,updated_at      datetime
    ,primary key(id)
) auto_increment = 10000;

create table resource_tic_user (
     id              bigint       not null auto_increment
    ,name            varchar(300) not null
    ,method_type     varchar( 30) not null
    ,scope_type      varchar( 30) not null
    ,permission_type varchar( 30)
    ,description     varchar(500)
    ,created_by      bigint       not null
    ,created_at      datetime     not null
    ,updated_by      bigint
    ,updated_at      datetime
    ,primary key(id)
) auto_increment = 10000;

create table resource_tic_admin_role (
     resource_id bigint not null
    ,role_id     bigint not null
    ,primary key(resource_id, role_id)
    ,foreign key(resource_id) references resource_tic_admin(id)
    ,foreign key(role_id)     references role(id)
);

create table resource_tic_user_role (
     resource_id bigint not null
    ,role_id     bigint not null
    ,primary key(resource_id, role_id)
    ,foreign key(resource_id) references resource_tic_user(id)
    ,foreign key(role_id)     references role(id)
);

create table resource_tic_auth_role (
     resource_id bigint not null
    ,role_id     bigint not null
    ,primary key(resource_id, role_id)
    ,foreign key(resource_id) references resource_tic_auth(id)
    ,foreign key(role_id)     references role(id)
);