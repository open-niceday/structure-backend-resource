package io.niceday.common.engine.aop;

import io.niceday.common.engine.config.integrate.common.exception.RestTemplateException;
import io.niceday.common.engine.exception.common.BadRequestExceptionForm;
import io.niceday.common.engine.exception.common.ExceptionCode;
import io.niceday.common.engine.exception.common.ExceptionForm;
import io.niceday.common.engine.exception.common.NotAcceptableException;
import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.common.engine.helper.message.MessageHelper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.persistence.EntityNotFoundException;

/**  
 * @since       2021.02.01
 * @author      lucas
 * @description exception handler(reference site : http://onecellboy.tistory.com/346)
 **********************************************************************************************************************/
@RestControllerAdvice
public class ExceptionsHandler {

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<?> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception){
        return new ResponseEntity<>(ExceptionForm.create(ExceptionCode.E00010001, MessageHelper.getMessage(ExceptionCode.E00010001)), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<?> handleValidationException(MethodArgumentNotValidException exception){
		return new ResponseEntity<>(BadRequestExceptionForm.create(exception, ExceptionCode.E00010001), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(BindException.class)
	public ResponseEntity<?> handleBindException(BindException exception){
		return new ResponseEntity<>(BadRequestExceptionForm.create(exception, ExceptionCode.E00010001), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<?> handleEntityNotFoundException(EntityNotFoundException exception){
		return new ResponseEntity<>(ExceptionForm.create(ExceptionCode.E00010002, MessageHelper.getMessage(ExceptionCode.E00010002)), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<?> handleNoDataException(NotFoundException exception){
		return new ResponseEntity<>(ExceptionForm.create(exception.getCode(), MessageHelper.getMessage(exception.getCode())),HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(NotAcceptableException.class)
	public ResponseEntity<?> handleNotAcceptableException(NotAcceptableException exception){
		return new ResponseEntity<>(ExceptionForm.create(exception.getCode(), MessageHelper.getMessage(exception.getCode())), HttpStatus.NOT_ACCEPTABLE);
	}

    @ExceptionHandler(RestTemplateException.class)
    public ResponseEntity<?> handleRestTemplateErrorException(RestTemplateException exception) {
        return new ResponseEntity<>(ExceptionForm.create(exception.getCode(), MessageHelper.getMessage(exception.getCode())), HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(ResourceAccessException.class)
    public ResponseEntity<?> handleResourceAccessException(ResourceAccessException exception) {
        return new ResponseEntity<>(ExceptionForm.create(ExceptionCode.E00100100, MessageHelper.getMessage(ExceptionCode.E00100100)), HttpStatus.SERVICE_UNAVAILABLE);
    }
}
