package io.niceday.milo.alone.comment.service;

import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.milo.alone.comment.entity.Comment;
import io.niceday.milo.alone.comment.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import com.querydsl.core.types.Predicate;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description comment service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class CommentService {
    
    private final CommentRepository commentRepository;

    @Transactional(readOnly=true)
    public List<Comment> getAll(Predicate predicate) {
        return commentRepository.findAll(predicate);
    }

    @Transactional(readOnly=true)
    public Comment get(Long commentId) {
        return commentRepository.findById(commentId).orElseThrow(NotFoundException::new);
    }
    
    public Comment add(Comment comment) {
        return commentRepository.save(comment);
    }
    
    public void remove(Long commentId) {
        commentRepository.delete(get(commentId));
    }
}