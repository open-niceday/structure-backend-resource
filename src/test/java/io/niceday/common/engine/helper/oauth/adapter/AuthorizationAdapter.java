package io.niceday.common.engine.helper.oauth.adapter;

import com.google.common.collect.ImmutableMap;
import io.niceday.common.engine.helper.model.ObjectHelper;
import io.niceday.common.engine.helper.oauth.form.TokenForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;
import java.util.Map;

/**
 * @since       2020.03.03
 * @author      lucas
 * @description authorization adapter
 **********************************************************************************************************************/
@Component
public class AuthorizationAdapter {

	private static RestTemplate restTemplate;

	private static String grantType;
	private static String clientId;
	private static String clientPw;
	private static String loginType;
	private static String checkTokenUrl;

	@Autowired
	private AuthorizationAdapter( @Value("${security.oauth2.client-id}"    )   String clientId
								 ,@Value("${security.oauth2.client-secret}")   String clientPw
								 ,@Value("${security.oauth2.check-token-url}") String checkTokenUrl
								 ,RestTemplate restTemplate) {

		AuthorizationAdapter.grantType     = "password";
		AuthorizationAdapter.loginType     = "TEST";
		AuthorizationAdapter.clientId      = clientId;
		AuthorizationAdapter.clientPw      = clientPw;
		AuthorizationAdapter.checkTokenUrl = checkTokenUrl.replaceAll("check_token", "token");
		AuthorizationAdapter.restTemplate  = restTemplate;
	}

	public static TokenForm.Response getToken(String username, String password){
		ResponseEntity<TokenForm.Response> result = restTemplate.postForEntity(checkTokenUrl, getHttpEntityOfAccessToken(username, password), TokenForm.Response.class);
		return result.getBody();
	}

	public static String getAuthorization(String username, String password){
		TokenForm.Response response = getToken(username, password);
		return String.format( "Bearer %s", response.getAccessToken());
	}

	private static String getBasic(String cliendId, String clientPw){
		String client = String.format("%s:%s", cliendId, clientPw);
		return String.format( "Basic %s", Base64.getEncoder().encodeToString(client.getBytes()));
	}

	private static HttpEntity getHttpEntityOfAccessToken(String username, String password){
		Map           headers = ImmutableMap.of(HttpHeaders.AUTHORIZATION, getBasic(clientId, clientPw), HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
		MultiValueMap params  = ObjectHelper.toParams(
				TokenForm.Request.builder()
						.grantType(grantType)
						.loginType(loginType)
						.clientId (clientId)
						.username (username)
						.password (password)
						.build());

		return getHttpEntity(headers, params);
	}

    private static HttpEntity getHttpEntity(Map<String, String> headers, Map<String, String> params) {
        HttpHeaders header = new HttpHeaders();
        for(Map.Entry<String, String> entry : headers.entrySet()){
            header.add(entry.getKey(), entry.getValue());
        }
        return new HttpEntity(params, header);
    }
}