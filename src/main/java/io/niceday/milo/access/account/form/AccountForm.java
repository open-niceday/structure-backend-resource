package io.niceday.milo.access.account.form;

import io.niceday.milo.access.account.enumerate.AccountStatus;
import io.niceday.milo.access.role.enumerate.RoleType;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @since       2021.03.09
 * @author      lucas
 * @description account form
 **********************************************************************************************************************/
public class AccountForm {

	public static class Request {

		@Getter
		@Setter
		@Builder(toBuilder=true)
		@NoArgsConstructor
		@AllArgsConstructor
		public static class Account {

			@ApiModelProperty(value="사용자일련번호", required=true)
			@NotNull
			private Long id;
		}
	}

	public static class Response {

		@Data
		public static class Account {

			@ApiModelProperty(value="사용자일련번호")
			private Long id;

			@ApiModelProperty(value="사용자상태")
			private AccountStatus accountStatus;

			@ApiModelProperty(value="이름")
			private String name;

			@ApiModelProperty(value="이메일")
			private String email;

			@ApiModelProperty(value="권한목록")
			private List<Role> roles;
		}

		@Data
		public static class Role {

			@ApiModelProperty(value="권한일련번호")
			private Long id;

			@ApiModelProperty(value="이름")
			private RoleType roleType;

			@ApiModelProperty(value="설명")
			private String description;
		}

		@Data
		@Builder
		public static class Principal {

			@ApiModelProperty(value="사용자일련번호")
			private Long id;

			@ApiModelProperty(value="사용자상태")
			private AccountStatus accountStatus;

			@ApiModelProperty(value="이름")
			private String name;

			@ApiModelProperty(value="이메일")
			private String email;

			@ApiModelProperty(value="권한목록")
			private List<Role> roles;
		}
	}
}
