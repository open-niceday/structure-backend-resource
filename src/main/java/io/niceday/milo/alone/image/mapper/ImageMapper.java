package io.niceday.milo.alone.image.mapper;

import io.niceday.milo.alone.image.entity.Image;
import io.niceday.milo.alone.image.form.ImageForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description image mapper
 **********************************************************************************************************************/
@Mapper
public interface ImageMapper {

	ImageMapper mapper = Mappers.getMapper(ImageMapper.class);

	Response.FindOne toFindOne(Image entity);
	Response.FindAll toFindAll(Image entity);
}
