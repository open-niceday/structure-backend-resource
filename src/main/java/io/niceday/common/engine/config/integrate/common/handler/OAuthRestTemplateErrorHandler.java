package io.niceday.common.engine.config.integrate.common.handler;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.common.engine.helper.context.RequestHelper;
import io.niceday.common.engine.helper.model.ObjectHelper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

/**   
 * @since       2020.04.01
 * @author      lucas
 * @description oauth rest template error handler
 **********************************************************************************************************************/
@Slf4j
@Component
public class OAuthRestTemplateErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        return httpResponse.getStatusCode().is5xxServerError() || httpResponse.getStatusCode().is4xxClientError();
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException {
        HttpServletResponse response = RequestHelper.getResponse();
        response.setStatus(HttpStatus.UNAUTHORIZED.value());

        OutputStream out = response.getOutputStream();
        ObjectHelper.outputStream(out, OAuthErrorForm.builder()
                .error           ("invalid_request")
                .errorDescription("invalid_token")
                .build());
        out.flush();
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OAuthErrorForm {

        @Description("에러")
        private String error;

        @Description("에러설명")
        @JsonProperty("error_description")
        private String errorDescription;
    }
}