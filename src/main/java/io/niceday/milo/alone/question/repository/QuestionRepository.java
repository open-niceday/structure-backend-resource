package io.niceday.milo.alone.question.repository;

import com.querydsl.core.types.Predicate;
import io.niceday.milo.alone.question.entity.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description question repository
 **********************************************************************************************************************/
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long>, QuerydslPredicateExecutor<Question> {

    @EntityGraph(attributePaths={"creator", "attach"})
    Page<Question> findAll(Predicate predicate, Pageable pageable);
}
