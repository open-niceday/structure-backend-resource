package io.niceday.milo.access.menu.repository;

import io.niceday.milo.access.menu.entity.Menu;
import io.niceday.milo.access.role.entity.Role;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @since       2022.06.02
 * @author      mushi
 * @description menu repository
 **********************************************************************************************************************/
@Repository
public interface MenuRepository extends JpaRepository<Menu, Long>, QuerydslPredicateExecutor<Menu> {

    @EntityGraph(attributePaths = {"creator", "updator", "children.creator", "children.updator"})
    List<Menu> findDistinctByParentIsNullAndRolesInOrderBySortAsc(List<Role> roles);
}
