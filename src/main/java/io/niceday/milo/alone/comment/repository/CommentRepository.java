package io.niceday.milo.alone.comment.repository;

import io.niceday.milo.alone.comment.entity.Comment;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import com.querydsl.core.types.Predicate;

import java.util.List;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description comment repository
 **********************************************************************************************************************/
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>, QuerydslPredicateExecutor<Comment> {

    @EntityGraph(attributePaths={"creator"})
    List<Comment> findAll(Predicate predicate);
}
