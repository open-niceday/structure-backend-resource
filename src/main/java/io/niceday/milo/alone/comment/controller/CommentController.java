package io.niceday.milo.alone.comment.controller;

import io.niceday.milo.alone.comment.adapter.CommentAdapter;
import io.niceday.milo.alone.comment.form.CommentForm;
import io.niceday.milo.alone.comment.form.CommentForm.Response;
import io.niceday.milo.alone.comment.mapper.CommentMapper;
import io.niceday.milo.alone.comment.predicate.CommentPredicate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.niceday.milo.alone.comment.mapper.CommentMapper.mapper;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description comment controller
 **********************************************************************************************************************/
@Api(description="댓글")
@RestController
@RequestMapping("${property.api.end-point}")
@RequiredArgsConstructor
public class CommentController {
    
    private final CommentAdapter commentAdapter;

    @ApiOperation("목록-문의")
    @GetMapping("/comments/questions/{questionId}")
    public List<Response.FindAll> getAll(@PathVariable Long questionId) {
        return Optional.ofNullable(commentAdapter.getAll(CommentPredicate.question(questionId))).orElse(Collections.emptyList()).stream()
                .map(CommentMapper.mapper::toFindAll)
                .collect(Collectors.toList());
    }

    @ApiOperation("등록-문의")
    @PostMapping("/comments/questions/{questionId}")
    public Response.FindOne add(@PathVariable Long questionId, @Valid @RequestBody CommentForm.Request.Add add){
        return mapper.toFindOne(commentAdapter.add(questionId, mapper.toEntity(add)));
    }

    @ApiOperation("삭제-문의")
    @DeleteMapping("/comments/{commentId}/questions/{questionId}")
    public void remove(@PathVariable Long questionId, @PathVariable Long commentId){
        commentAdapter.remove(questionId, commentId);
    }
}
