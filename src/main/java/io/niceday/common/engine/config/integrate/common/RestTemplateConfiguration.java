package io.niceday.common.engine.config.integrate.common;

import com.google.common.collect.ImmutableList;
import io.niceday.common.engine.config.integrate.common.handler.OAuthRestTemplateErrorHandler;
import io.niceday.common.engine.config.integrate.common.handler.RestTemplateErrorHandler;
import io.niceday.common.engine.config.integrate.common.interceptor.AuthenticatedRestTemplateInterceptor;
import lombok.RequiredArgsConstructor;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**   
 * @since       2021.03.10
 * @author      lucas
 * @description rest template configuration
 **********************************************************************************************************************/
@RequiredArgsConstructor
@Configuration
public class RestTemplateConfiguration {

    private final RestTemplateErrorHandler             restTemplateErrorHandler;
    private final OAuthRestTemplateErrorHandler        oauthRestTemplateErrorHandler;
    private final AuthenticatedRestTemplateInterceptor restTemplateInterceptor;

    @Bean
    public HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory(){
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectTimeout(10000);
        httpRequestFactory.setReadTimeout   ( 5000);
        httpRequestFactory.setHttpClient    (HttpClientBuilder.create()
                .setMaxConnTotal   (200)
                .setMaxConnPerRoute(100)
                .build());
        return httpRequestFactory;
    }

    @Bean
    public RestTemplate restTemplate(){
        RestTemplate restTemplate = new RestTemplate(httpComponentsClientHttpRequestFactory());
        restTemplate.setErrorHandler(restTemplateErrorHandler);
        restTemplate.setInterceptors(ImmutableList.of(restTemplateInterceptor));
        return restTemplate;
    }

    @Bean
    public RestTemplate oAuthRestTemplate(){
        RestTemplate restTemplate = new RestTemplate(httpComponentsClientHttpRequestFactory());
        restTemplate.setErrorHandler(oauthRestTemplateErrorHandler);
        restTemplate.setInterceptors(ImmutableList.of(restTemplateInterceptor));
        return restTemplate;
    }
}