package io.niceday.milo.alone.attach.validator;

import io.niceday.milo.alone.attach.service.AttachService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @since       2022.12.01
 * @author      lucas
 * @description attach validator
 **********************************************************************************************************************/
@Component
public class AttachValidator {

    private static AttachService attachService;

    @Autowired
    private AttachValidator(AttachService attachService) {
        AttachValidator.attachService = attachService;
    }

    public static boolean isValid(String attachId) {
        return StringUtils.isBlank(attachId) ? Boolean.TRUE : attachService.exists(attachId);
    }
}
