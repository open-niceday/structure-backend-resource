create table faq (
     id            bigint        not null auto_increment
    ,faq_type      varchar(30)   not null
    ,title         varchar(1000) not null
    ,content       text          not null
    ,sort          bigint
    ,created_by    bigint        not null
    ,created_at    datetime      not null
    ,updated_by    bigint
    ,updated_at    datetime
    ,primary key(id)
) auto_increment = 10000;

create table notice (
     id            bigint        not null auto_increment
    ,notice_type   varchar(30)   not null
    ,title         varchar(1000) not null
    ,content       text          not null
    ,created_by    bigint        not null
    ,created_at    datetime      not null
    ,updated_by    bigint
    ,updated_at    datetime
    ,primary key(id)
) auto_increment = 10000;

create table question (
     id            bigint        not null auto_increment
    ,question_type varchar(30)   not null
    ,title         varchar(1000) not null
    ,content       text          not null
    ,answer_yn     boolean       not null
    ,created_by    bigint        not null
    ,created_at    datetime      not null
    ,updated_by    bigint
    ,updated_at    datetime
    ,primary key(id)
) auto_increment = 10000;

create table comment (
     id            bigint        not null auto_increment
    ,content       text          not null
    ,created_by    bigint        not null
    ,created_at    datetime      not null
    ,updated_by    bigint
    ,updated_at    datetime
    ,primary key(id)
) auto_increment = 10000;