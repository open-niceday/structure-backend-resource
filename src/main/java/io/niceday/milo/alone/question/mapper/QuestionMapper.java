package io.niceday.milo.alone.question.mapper;

import io.niceday.milo.alone.question.entity.Question;
import io.niceday.milo.alone.question.form.QuestionForm.Request;
import io.niceday.milo.alone.question.form.QuestionForm.Response;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description question mapper
 **********************************************************************************************************************/
@Mapper
public interface QuestionMapper {
    
    QuestionMapper mapper = Mappers.getMapper(QuestionMapper.class);
    
    Response.FindAll toFindAll(Question entity);
    Response.FindOne toFindOne(Question entity);

    Question toEntity(@Context QuestionContext context, Request.Add    form);
    Question toEntity(@Context QuestionContext context, Request.Modify form, Long id);

    @Mappings({
         @Mapping(target="id",       ignore=true)
        ,@Mapping(target="answerYn", ignore=true)
    })
    Question modify(Question source, @MappingTarget Question target);
}
