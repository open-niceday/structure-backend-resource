package io.niceday.common.engine.config.auditing;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**   
 * @since       2021.02.01
 * @author      lucas
 * @description auditing configuration
 **********************************************************************************************************************/
@Configuration 
@EnableJpaAuditing
public class AuditingConfiguration { 
	
}
