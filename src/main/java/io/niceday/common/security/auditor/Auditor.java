package io.niceday.common.security.auditor;

import io.niceday.common.security.principal.helper.PrincipalHelper;
import io.niceday.milo.access.account.entity.Account;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * @since       2021.03.30
 * @author      lucas
 * @description auditor
 **********************************************************************************************************************/
@Component
@RequiredArgsConstructor
public class Auditor implements AuditorAware<Account> {

    @Override
    public Optional<Account> getCurrentAuditor() {
        Account account = PrincipalHelper.getAccount();
        return ObjectUtils.allNotNull(account) ? Optional.of(account) : Optional.empty();
    }
}