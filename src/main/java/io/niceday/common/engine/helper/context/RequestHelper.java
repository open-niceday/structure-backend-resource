package io.niceday.common.engine.helper.context;

import com.google.common.collect.ImmutableList;
import io.niceday.common.engine.constant.Constant;
import io.niceday.common.security.principal.helper.PrincipalHelper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**   
 * @since       2021.02.01
 * @author      lucas
 * @description request helper
 **********************************************************************************************************************/
@Slf4j
@Component
public class RequestHelper {

    private static final ImmutableList<String> IP_HEADER_CANDIDATES = ImmutableList.of("X-Forwarded-For"
                                                                                    , "Proxy-Client-IP"
                                                                                    , "WL-Proxy-Client-IP"
                                                                                    , "HTTP_X_FORWARDED_FOR"
                                                                                    , "HTTP_X_FORWARDED"
                                                                                    , "HTTP_X_CLUSTER_CLIENT_IP"
                                                                                    , "HTTP_CLIENT_IP"
                                                                                    , "HTTP_FORWARDED_FOR"
                                                                                    , "HTTP_FORWARDED"
                                                                                    , "HTTP_VIA"
                                                                                    , "REMOTE_ADDR");


    public static HttpServletRequest getRequest(){
		return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
	}

	public static HttpServletResponse getResponse(){
		return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getResponse();
	}

	@SneakyThrows
	public static void currentRequestPrint() {

		HttpServletRequest request   = getRequest();
		String             method    = request.getMethod();
		StringBuffer       url       = request.getRequestURL();
		Long               accountId = PrincipalHelper.getAccount().getId();

		log.info("method={}, url={}, account={}", method, url, accountId);
	}

    @SneakyThrows
    public static String currentRequestAgent() {
        return getRequest().getHeader("user-agent");
    }

    public static String getClientIp() {

        return getHeaders().entrySet().stream()
                .filter   (header -> IP_HEADER_CANDIDATES.stream().anyMatch(candidate -> StringUtils.equalsIgnoreCase(candidate, Objects.toString(header.getKey()))))
                .findFirst()
                .map      (header -> Objects.toString(header.getValue()).split(Constant.String.COMMA)[0])
                .orElse   (getRequest().getRemoteAddr());
    }

    public static Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        Collections.list(getRequest().getHeaderNames())
                .forEach(key -> headers.put(key, getRequest().getHeader(key)));
        return headers;
    }
}
