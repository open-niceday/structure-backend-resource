package io.niceday.milo.access.menu.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @since       2022.06.02
 * @author      mushi
 * @description menu form
 **********************************************************************************************************************/
public class MenuForm {

    public static class Response {

        @Data
        public static class FindOne {

            @ApiModelProperty(value="메뉴일련번호")
            private Long id;

            @ApiModelProperty(value="이름")
            private String name;

            @ApiModelProperty(value="순서")
            private Integer sort;

            @ApiModelProperty(value="링크")
            private String link;

            @ApiModelProperty(value="하위-메뉴")
            private List<FindOne> children;
        }
    }
}
