package io.niceday.milo.access.role.repository;

import com.querydsl.core.types.Predicate;
import io.niceday.milo.access.role.entity.Role;
import io.niceday.milo.access.role.enumerate.RoleType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description role repository
 **********************************************************************************************************************/
@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, QuerydslPredicateExecutor<Role> {

    Boolean existsByRoleType(RoleType type);

    @EntityGraph(attributePaths={"creator", "creator.profile"})
    Page<Role> findAll(Predicate predicate, Pageable pageable);
}