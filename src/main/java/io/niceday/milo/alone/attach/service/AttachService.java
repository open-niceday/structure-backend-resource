package io.niceday.milo.alone.attach.service;

import io.niceday.common.engine.exception.common.NotFoundException;
import io.niceday.milo.alone.attach.entity.Attach;
import io.niceday.milo.alone.attach.repository.AttachRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description attach service
 **********************************************************************************************************************/
@Service
@Transactional
@RequiredArgsConstructor
public class AttachService {

	private final AttachRepository attachRepository;

	@Transactional(readOnly=true)
	public Page<Attach> getPage(Pageable pageable) {
		return attachRepository.findAll(pageable);
	}

	@Transactional(readOnly=true)
	public Attach get(String attachId) {
		return attachRepository.findById(attachId).orElseThrow(NotFoundException::new);
	}

	@Transactional(readOnly=true)
	public boolean exists(String attachId) {
		return attachRepository.existsById(attachId);
	}

	public Attach add(Attach attach) {
		return attachRepository.save(attach);
	}

	public void remove(String attachId) {
		attachRepository.delete(get(attachId));
	}
}
