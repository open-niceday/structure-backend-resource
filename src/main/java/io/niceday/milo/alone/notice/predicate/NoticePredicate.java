package io.niceday.milo.alone.notice.predicate;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import io.niceday.milo.alone.notice.entity.QNotice;
import io.niceday.milo.alone.notice.form.NoticeForm.Request;

import java.util.Optional;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description notice predicate
 **********************************************************************************************************************/
public class NoticePredicate {

   public static Predicate search(Request.Find find){

       QNotice        notice  = QNotice.notice;
       BooleanBuilder builder = new BooleanBuilder();

       Optional.ofNullable(find.getNoticeType()).ifPresent(p -> builder.and(notice.noticeType.eq(p)));
       Optional.ofNullable(find.getKeyword   ()).ifPresent(p -> builder.and(notice.title.startsWith(p).or(notice.creator.name.startsWith(p))));

       return builder;
   }
}
