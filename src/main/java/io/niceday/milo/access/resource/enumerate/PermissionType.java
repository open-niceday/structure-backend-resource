package io.niceday.milo.access.resource.enumerate;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description 허용 타입
 *               - PERMIT : 권한이 없어도 허용
 *               - DENY   : 권한이 존재해야 허용
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum PermissionType {

     PERMIT("모두허용")
    ,DENY  ("권한허용");

    private String description;
}