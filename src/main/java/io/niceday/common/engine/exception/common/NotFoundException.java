package io.niceday.common.engine.exception.common;

import lombok.Getter;

/**
 * @since       2021.02.01
 * @author      lucas
 * @description not found exception(데이터 조회를 하지 못했을 경우 발생되는 예외)
 **********************************************************************************************************************/
@SuppressWarnings("serial")
public class NotFoundException extends RuntimeException {

	@Getter
	public ExceptionCode code;

	public NotFoundException(){
		super(ExceptionCode.E00010002.name());
		code = ExceptionCode.E00010002;
	}
	
	public NotFoundException(ExceptionCode exceptionCode){
		super(exceptionCode.name());
		code = exceptionCode;
	}
	
	public NotFoundException(ExceptionCode exceptionCode, Exception exception){
		super(exceptionCode.name(), exception);
		code = exceptionCode;
	}
}
