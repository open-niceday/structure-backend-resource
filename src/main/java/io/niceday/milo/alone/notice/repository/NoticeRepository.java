package io.niceday.milo.alone.notice.repository;

import com.querydsl.core.types.Predicate;
import io.niceday.milo.alone.notice.entity.Notice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description notice repository
 **********************************************************************************************************************/
@Repository
public interface NoticeRepository extends JpaRepository<Notice, Long>, QuerydslPredicateExecutor<Notice> {

    @EntityGraph(attributePaths={"creator", "attach"})
    Page<Notice> findAll(Predicate predicate, Pageable pageable);
}
