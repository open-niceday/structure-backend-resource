package io.niceday.milo.alone.image.bridge;

import io.niceday.common.engine.config.integrate.aws.s3.S3Content;
import io.niceday.common.engine.config.integrate.aws.s3.S3Integration;
import io.niceday.common.engine.config.integrate.aws.s3.S3Type;
import io.niceday.milo.alone.image.entity.Image;
import io.niceday.milo.alone.image.service.ImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description image bridge
 **********************************************************************************************************************/
@Component
@RequiredArgsConstructor
public class ImageBridge {

    private final ImageService imageService;

    public Image add(MultipartFile file) {
        S3Content content = S3Integration.add(S3Type.IMAGE, file);
        return Image.builder()
                .id  (content.getId())
                .name(content.getName())
                .path(content.getPath())
                .size(content.getSize())
                .build();
    }

    public void remove(String imageId) {
        Image image = imageService.get(imageId);
        S3Integration.remove(image.getPath());
    }
}
