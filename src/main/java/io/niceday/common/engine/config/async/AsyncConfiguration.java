package io.niceday.common.engine.config.async;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**   
 * @since       2021.08.11
 * @author      lucas
 * @description async configuration
 **********************************************************************************************************************/
@Configuration 
@EnableAsync
public class AsyncConfiguration {
	
}
