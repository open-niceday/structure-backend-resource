package io.niceday.common.base.validator;

import io.niceday.common.base.enumerate.DisplayStatus;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @since       2021.08.04
 * @author      lucas
 * @description display validator
 **********************************************************************************************************************/
public class DisplayValidator {

	public static boolean isValid(final DisplayStatus status, LocalDateTime openDate){
		if(DisplayStatus.RESERVE.equals(status)){
			return Objects.nonNull(openDate);
		}
		return Boolean.TRUE;
    }
}
