package io.niceday.milo.alone.faq.mapper;

import io.niceday.milo.alone.faq.entity.Faq;
import io.niceday.milo.alone.faq.form.FaqForm.Request;
import io.niceday.milo.alone.faq.form.FaqForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description faq mapper
 **********************************************************************************************************************/
@Mapper
public interface FaqMapper {
    
    FaqMapper mapper = Mappers.getMapper(FaqMapper.class);
    
    Response.FindAll toFindAll(Faq entity);
    Response.FindOne toFindOne(Faq entity);

    Faq toEntity(Request.Add form);
    Faq toEntity(Request.Modify form, Long id);

    @Mappings({
         @Mapping(target="id",   ignore=true)
        ,@Mapping(target="sort", ignore=true)
    })
    Faq modify(Faq source, @MappingTarget Faq target);
}
