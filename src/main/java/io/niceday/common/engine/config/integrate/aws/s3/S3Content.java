package io.niceday.common.engine.config.integrate.aws.s3;

import io.niceday.common.engine.annotation.entity.Description;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description s3 content
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Description("첨부")
public class S3Content {

    @Description("첨부일련번호")
    private String id;

    @Description("원본이름")
    private String name;

    @Description("경로")
    private String path;

    @Description("크기")
    private Long size;
}