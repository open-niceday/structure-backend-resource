package io.niceday.milo.alone.question.controller;

import io.niceday.milo.alone.question.form.QuestionForm;
import io.niceday.milo.alone.question.form.QuestionForm.Response;
import io.niceday.milo.alone.question.mapper.QuestionContext;
import io.niceday.milo.alone.question.predicate.QuestionPredicate;
import io.niceday.milo.alone.question.service.QuestionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static io.niceday.milo.alone.question.mapper.QuestionMapper.mapper;

/**
 * @since       2022.11.30
 * @author      lucas
 * @description question controller
 **********************************************************************************************************************/
@Api(description="문의")
@RestController
@RequestMapping("${property.api.end-point}")
@RequiredArgsConstructor
public class QuestionController {
    
    private final QuestionService questionService;

    @ApiOperation("목록")
    @GetMapping("/questions/pages")
    public Page<Response.FindAll> getPage(@Valid QuestionForm.Request.Find find, @PageableDefault Pageable pageable) {
        return questionService.getPage(QuestionPredicate.search(find), pageable).map(mapper::toFindAll);
    }
    
    @ApiOperation("조회")
    @GetMapping("/questions/{questionId}")
    public Response.FindOne get(@PathVariable Long questionId) {
        return mapper.toFindOne(questionService.get(questionId));
    }
    
    @ApiOperation("등록")
    @PostMapping("/questions")
    public Response.FindOne add(@Valid @RequestBody QuestionForm.Request.Add add){
        return mapper.toFindOne(questionService.add(mapper.toEntity(QuestionContext.create(), add)));
    }
    
    @ApiOperation("수정")
    @PutMapping("/questions/{questionId}")
    public Response.FindOne modify(@PathVariable Long questionId, @Valid @RequestBody QuestionForm.Request.Modify modify){
        return mapper.toFindOne(questionService.modify(mapper.toEntity(QuestionContext.create(), modify, questionId), questionId));
    }

    @ApiOperation("삭제")
    @DeleteMapping("/questions/{questionId}")
    public void remove(@PathVariable Long questionId){
        questionService.remove(questionId);
    }
}
