package io.niceday.milo.access.role.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import io.niceday.milo.access.menu.entity.Menu;
import io.niceday.milo.access.role.enumerate.RoleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description role
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("권한")
public class Role extends Base {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Description("권한일련번호")
    @Column(nullable=false)
    private Long id;

    @Description("권한구분")
    @Enumerated(EnumType.STRING)
    @Column(nullable=false, length=30)
    private RoleType roleType;

    @Description("설명")
    @Column(nullable=false, length=500)
    private String description;


    @Description("메뉴목록")
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(name="role_menu", joinColumns=@JoinColumn(name="role_id"), inverseJoinColumns=@JoinColumn(name="menu_id"))
    private List<Menu> menus;
}
