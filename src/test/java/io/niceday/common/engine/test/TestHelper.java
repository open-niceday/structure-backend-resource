package io.niceday.common.engine.test;

import com.google.common.collect.Maps;
import io.niceday.common.engine.helper.path.PathHelper;
import io.niceday.milo.access.account.Accounts;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * @since       2021.02.01
 * @author      lucas
 * @description test helper
 **********************************************************************************************************************/
public class TestHelper {

	public static MockMvc 						 mock;
	public static RestDocumentationResultHandler handler;
	public static Map<Accounts, String> 		 accounts = Maps.newHashMap();


	private static String getAuthorization(Accounts account){
		return accounts.get(account);
	}

	public static void initialize(MockMvc _mock, RestDocumentationResultHandler _handler){
		mock     = _mock;
		handler  = _handler;

		if(CollectionUtils.isEmpty(accounts)){
			for(Accounts account : Accounts.values()){
				accounts.put(account, account.getAuthorization());
			}
		}
	}

	public static MockHttpServletRequestBuilder get(String url){
		return get(Accounts.LUCAS, url);
	}

	public static MockHttpServletRequestBuilder get(String url, Object ... params){
		return get(Accounts.LUCAS, url, params);
	}

	public static MockHttpServletRequestBuilder post(String url){
		return post(Accounts.LUCAS, url);
	}

	public static MockHttpServletRequestBuilder post(String url, Object ... params){
		return post(Accounts.LUCAS, url, params);
	}

	public static MockHttpServletRequestBuilder put(String url, Object ... params){
		return put(Accounts.LUCAS, url, params);
	}

	public static MockHttpServletRequestBuilder patch(String url, Object ... params){
		return patch(Accounts.LUCAS, url, params);
	}

	public static MockHttpServletRequestBuilder delete(String url, Object ... params){
		return delete(Accounts.LUCAS, url, params);
	}

	public static MockHttpServletRequestBuilder file(String url, String filePath, Object ... params) {
		return file(Accounts.LUCAS, url, filePath, params);
	}

	public static MockHttpServletRequestBuilder get(Accounts account, String url){
		return RestDocumentationRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, getAuthorization(account));
	}

	public static MockHttpServletRequestBuilder get(Accounts account, String url, Object ... params){
		return RestDocumentationRequestBuilders.get(url, params).contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, getAuthorization(account));
	}

	public static MockHttpServletRequestBuilder post(Accounts account, String url){
		return RestDocumentationRequestBuilders.post(url).contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, getAuthorization(account));
	}

	public static MockHttpServletRequestBuilder post(Accounts account, String url, Object ... params){
		return RestDocumentationRequestBuilders.post(url, params).contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, getAuthorization(account));
	}

	public static MockHttpServletRequestBuilder put(Accounts account, String url, Object ... params){
		return RestDocumentationRequestBuilders.put(url, params).contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, getAuthorization(account));
	}

	public static MockHttpServletRequestBuilder patch(Accounts account, String url, Object ... params){
		return RestDocumentationRequestBuilders.patch(url, params).contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, getAuthorization(account));
	}

	public static MockHttpServletRequestBuilder delete(Accounts account, String url, Object ... params){
		return RestDocumentationRequestBuilders.delete(url, params).contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, getAuthorization(account));
	}

	@SneakyThrows
	public static MockHttpServletRequestBuilder file(Accounts account, String url, String filePath, Object ... params) {
		return RestDocumentationRequestBuilders
					.fileUpload(url, params)
					.file(new MockMultipartFile(
						  "file"
								,PathHelper.getPath(filePath).getName()
								,MediaType.MULTIPART_FORM_DATA_VALUE
								,FileUtils.openInputStream(PathHelper.getPath(filePath))))
					.contentType(MediaType.APPLICATION_JSON)
					.header(HttpHeaders.AUTHORIZATION, getAuthorization(account));
	}
}
