package io.niceday.common.engine.helper.api;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiOperation;
import io.niceday.common.engine.constant.Constant;
import io.niceday.common.engine.test.SuperTest;
import lombok.Builder;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.ToString;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpMethod;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @since       2021.03.25
 * @author      lucas
 * @description api test
 **********************************************************************************************************************/
@Transactional
public class ApiTest extends SuperTest {

	@Test
	public void t01_get_apis() {

		List<Api>    apis      = ApiMaker.getApis(new File("").getAbsolutePath() + "/build/resources/main/");
		List<String> resources = Lists.newArrayList();
		List<String> roles     = Lists.newArrayList();

		resources.add("insert into resource_tic_admin(id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (1, '/api/**', 'all', 'NONE', 'ALL', 'DENY', 1, now());");

		AtomicInteger atomic = new AtomicInteger(1);
		for(Api api : apis){
			resources.add(
					String.format("insert into resource_tic_admin(id, name, description, method_type, scope_type, permission_type, created_by, created_at) values (%s, '%s', '%s', '%s', 'METHOD', null, 1, now());"
							,atomic.incrementAndGet()
							,api.getUri()
							,String.format("%s", api.getClazz().getSimpleName().replaceAll("Controller", Constant.String.EMPTY).toLowerCase())
							,api.getMethod().name()
					)
			);
			roles.add(
					String.format("insert into resource_tic_admin_role(role_id, resource_id) values (1, %s);"
							,atomic.get()
					)
			);
		}

		System.out.println();
		System.out.println("-- resource");
		for(String resource : resources){
			System.out.println(resource);
		}

		System.out.println();
		System.out.println("-- resource.role");
		for(String role : roles){
			System.out.println(role);
		}
	}
}

@Getter
@Builder
@ToString
class Api {

	private Class      clazz;
	private HttpMethod method;
	private String     uri;
	private String     description;
}

class ApiMaker {

	@SneakyThrows
	public static List<Api> getApis(String rootPath) {
		final String ROOT_PATH        = rootPath;
		final String END_POINT        = "/api";
		final String FILTER_EXTENSION = "class";

		Collection<File> files = FileUtils.listFiles(
				FileUtils.getFile(ROOT_PATH)
				, ArrayUtils.toStringArray(ImmutableList.of(FILTER_EXTENSION).toArray())
				,Boolean.TRUE
		);

		List<Class> classes = Lists.newArrayList();
		for(File file : files){
			String packages = file.getAbsolutePath();
			packages = StringUtils.replace  (packages, ROOT_PATH, Constant.String.EMPTY);
			packages = StringUtils.substring(packages, Constant.Integer.ZERO, StringUtils.indexOf(packages, Constant.String.DOT));
			packages = StringUtils.replace  (packages, Constant.String.SLASH, Constant.String.DOT);

			if(packages.equals("io.niceday.common.engine.config.rabbit.RabbitConfiguration")) {
				System.out.println("test");
			}

			Class clazz = Class.forName(packages);
			if(Objects.nonNull(clazz.getAnnotation(RestController.class))){
				classes.add(clazz);
			}
		}

		List<Api> apis = Lists.newArrayList();
		for(Class clazz : classes){
			List<Method> methods = Arrays.asList(clazz.getMethods());
			for(Method method : methods){
				Annotation get         = method.getAnnotation(GetMapping.class   );
				Annotation post        = method.getAnnotation(PostMapping.class  );
				Annotation put         = method.getAnnotation(PutMapping.class   );
				Annotation patch       = method.getAnnotation(PatchMapping.class );
				Annotation delete      = method.getAnnotation(DeleteMapping.class);
				Annotation description = method.getAnnotation(ApiOperation.class);
				for(Annotation annotation : method.getAnnotations()){
					if(annotation.equals(get)) {
						apis.add(Api.builder()
								.clazz(clazz)
								.description(String.valueOf(AnnotationUtils.getValue(description)))
								.method(HttpMethod.GET)
								.uri(StringUtils.join(END_POINT, String[].class.cast(AnnotationUtils.getValue(get   ))[0]))
								.build());
					}

					if(annotation.equals(post)) {
						apis.add(Api.builder()
								.clazz(clazz)
								.description(String.valueOf(AnnotationUtils.getValue(description)))
								.method(HttpMethod.POST)
								.uri(StringUtils.join(END_POINT, String[].class.cast(AnnotationUtils.getValue(post  ))[0]))
								.build());
					}

					if(annotation.equals(put)) {
						apis.add(Api.builder()
								.clazz(clazz)
								.description(String.valueOf(AnnotationUtils.getValue(description)))
								.method(HttpMethod.PUT)
								.uri(StringUtils.join(END_POINT, String[].class.cast(AnnotationUtils.getValue(put   ))[0]))
								.build());
					}

					if(annotation.equals(patch)) {
						apis.add(Api.builder()
								.clazz(clazz)
								.description(String.valueOf(AnnotationUtils.getValue(description)))
								.method(HttpMethod.PATCH)
								.uri(StringUtils.join(END_POINT, String[].class.cast(AnnotationUtils.getValue(patch ))[0]))
								.build());
					}

					if(annotation.equals(delete)) {
						apis.add(Api.builder()
								.clazz(clazz)
								.description(String.valueOf(AnnotationUtils.getValue(description)))
								.method(HttpMethod.DELETE)
								.uri(StringUtils.join(END_POINT, String[].class.cast(AnnotationUtils.getValue(delete))[0]))
								.build());
					}
				}
			}
		}
		return apis;
	}
}