package io.niceday.milo.alone.image.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description image
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("이미지")
public class Image extends Base {

	@Id
	@Description("이미지일련번호")
	@Column(nullable=false)
	private String id;

	@Description("원본이름")
	@Column(nullable=false, length=500)
	private String name;

	@Description("경로")
	@Column(nullable=false, length=300)
	private String path;

	@Description("크기")
	@Column(nullable=false)
	private Long size;
}