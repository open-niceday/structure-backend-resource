package io.niceday.common.engine.config.integrate.aws.s3;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @since       2022.11.16
 * @author      lucas
 * @description s3 type
 **********************************************************************************************************************/
@Getter
@AllArgsConstructor
public enum S3Type {

     IMAGE ("이미지")
    ,ATTACH("첨부");

    private final String description;
}