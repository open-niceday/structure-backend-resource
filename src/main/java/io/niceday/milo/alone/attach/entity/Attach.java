package io.niceday.milo.alone.attach.entity;

import io.niceday.common.base.entity.Base;
import io.niceday.common.engine.annotation.entity.Description;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @since       2021.03.19
 * @author      lucas
 * @description attach
 **********************************************************************************************************************/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder=true)
@Entity
@Description("첨부")
public class Attach extends Base {

	@Id
	@Description("첨부일련번호")
	@Column(nullable=false)
	private String id;

	@Description("원본이름")
	@Column(nullable=false, length=500)
	private String name;

	@Description("경로")
	@Column(nullable=false, length=300)
	private String path;

	@Description("크기")
	@Column(nullable=false)
	private Long size;
}